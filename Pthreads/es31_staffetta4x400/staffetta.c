/*ex 31: staffetta.c*/

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <inttypes.h>
#include <pthread.h> 

#include "printerror.h"
#include "DBGpthread.h"

#define N_ATLETI 4

int atletaConStaffetta;

pthread_mutex_t mutex;
pthread_cond_t condTurno[N_ATLETI], condLascia, condParti;

void *Atleta(void* arg) {
	int i = (intptr_t)arg;
	char ident[32];
	
	sprintf(ident, "Atleta %d", i);
	
	while (1) {
		DBGpthread_mutex_lock(&mutex, ident);
		/*se siamo l'atleta con la staffetta procediamo, altrimenti aspettiamo
		 * sulla nostra waitTurno*/
		if (i!=atletaConStaffetta) {
			DBGpthread_cond_wait(&condTurno[i], &mutex, ident);
			/*quando questo atleta viene svegliato deve effettuare il passaggio di testimone 
			 * con l'atleta che lo ha chiamato*/
			printf("%s riceve la staffetta e dice al compagno di lasciarla\n", ident); fflush(stdout);
			DBGpthread_cond_signal(&condLascia, ident);
			/*ci mettiamo ad aspettare che quello che ci ha passato la staffetta ci dica che
			 * possiamo partire*/
			DBGpthread_cond_wait(&condParti, &mutex, ident);	
			/*ok: abbiamo la staffetta e possiamo partire*/
		}
		
		/*l'atleta con la staffetta corre*/
		DBGpthread_mutex_unlock(&mutex, ident);
		
		printf("%s corre\n", ident); fflush(stdout);
		
		DBGsleep(1, ident);
		
		DBGpthread_mutex_lock(&mutex, ident);
		printf("%s è arrivato al compagno successivo: %d\n", ident, (i+1)%N_ATLETI); fflush(stdout);
		
		/*chiamo il compagno successivo*/
		DBGpthread_cond_signal(&condTurno[(i+1)%N_ATLETI], ident);
		/*gli do la staffetta*/
		printf("%s passa la staffetta al compagno successivo\n", ident); fflush(stdout);
		/*mi metto ad aspettare che dica che va tutto ok*/
		DBGpthread_cond_wait(&condLascia, &mutex, ident);
		/*lasciamo definitivamente la staffetta*/
		atletaConStaffetta=(atletaConStaffetta+1)%N_ATLETI;
		/*segnaliamo al nostro compagno che può partire*/
		printf("%s urla: PARTI!\n", ident); fflush(stdout);
		DBGpthread_cond_signal(&condParti, ident);
		/*a questo punto il nostro lavoro è terminato: rilasciamo il lock su mutex
		 * e torniamo ad aspettare*/
		DBGpthread_mutex_unlock(&mutex, ident);
	}
	/*mai raggiunto*/
	pthread_exit(NULL);
}

int main (void) {
	char ident[32];
	int rc, i;
	pthread_t pth;
	intptr_t index;
	
	sprintf(ident, "main");

	/*initialize posix structures*/
	
	atletaConStaffetta=0;
	
	rc = pthread_mutex_init(&mutex, NULL);
	if ( rc ) PrintERROR_andExit(rc, ident);
	rc = pthread_cond_init(&condLascia, NULL);
	if ( rc ) PrintERROR_andExit(rc, ident);
	rc = pthread_cond_init(&condParti, NULL);
	if ( rc ) PrintERROR_andExit(rc, ident);
	for(i=0; i<N_ATLETI; i++) {
		rc = pthread_cond_init(&condTurno[i], NULL);
		if ( rc ) PrintERROR_andExit(rc, ident);
	}
	
	for(i=0; i<N_ATLETI; i++) {
		index = i;
		rc = pthread_create(&pth, NULL, Atleta, (void*)index);
		if (rc) PrintERROR_andExit(errno, ident);
	}

	pthread_exit(NULL);
	return 0;
}
