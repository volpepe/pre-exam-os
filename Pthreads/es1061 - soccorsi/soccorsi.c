#include "printerror.h"

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <inttypes.h>
#include <pthread.h> 

#include "DBGpthread.h"

#define NUMPROF 1
#define NUMEXOR 1
#define NUMMED 1

int caduto;
int medicoInAttesa;
int esorcistaInAttesa;
int medicoHoFatto;
int esorcistaHoFatto;

pthread_mutex_t mutex;
pthread_cond_t sync, wait4Fall, waitOperation;

void *Exorcist(void* arg) {
	int td = (intptr_t) arg;
	char ident[32];
	sprintf(ident, "Exorcist %d", td);

	while(1) {
		DBGpthread_mutex_lock(&mutex, ident);
		while(!caduto || esorcistaHoFatto) {
			/*solo il prof che cade può riattivare 
			questa wait*/
			DBGpthread_cond_wait(&wait4Fall, &mutex, ident);
		}

		esorcistaHoFatto = 0;
		if(!medicoInAttesa) {
			esorcistaInAttesa++;
			DBGpthread_cond_wait(&sync, &mutex, ident);
			esorcistaInAttesa--;
		} else {
			/*se il medico è in attesa obv lo sveglio*/
			DBGpthread_cond_signal(&sync, ident);
		}

		DBGpthread_mutex_unlock(&mutex, ident);
		/*interveniamo*/
		DBGsleep(2, ident);

		DBGpthread_mutex_lock(&mutex, ident);
		esorcistaHoFatto++;
		printf("Esorcista: ho fatto!\n");
		fflush(stdout);
		DBGpthread_cond_signal(&waitOperation, ident);
		DBGpthread_mutex_unlock(&mutex, ident);
	}
}

void *Medic(void* arg) {
	int td = (intptr_t) arg;
	char ident[32];
	sprintf(ident, "Medic %d", td);

	while(1) {
		DBGpthread_mutex_lock(&mutex, ident);
		while(!caduto || medicoHoFatto) {
			/*solo il prof che cade può riattivare 
			questa wait*/
			DBGpthread_cond_wait(&wait4Fall, &mutex, ident);
		}

		medicoHoFatto = 0;
		if(!esorcistaInAttesa) {
			medicoInAttesa++;
			DBGpthread_cond_wait(&sync, &mutex, ident);
			medicoInAttesa--;
		} else {
			/*se il medico è in attesa obv lo sveglio*/
			DBGpthread_cond_signal(&sync, ident);
		}

		DBGpthread_mutex_unlock(&mutex, ident);
		/*interveniamo*/
		DBGsleep(2, ident);

		DBGpthread_mutex_lock(&mutex, ident);
		medicoHoFatto++;
		printf("Medico: ho fatto!\n");
		fflush(stdout);
		DBGpthread_cond_signal(&waitOperation, ident);
		DBGpthread_mutex_unlock(&mutex, ident);
	}
}

void* Professor(void* arg) {
	int td = (intptr_t) arg;
	char ident[32];
	sprintf(ident, "Professor %d", td);

	while(1) {
		printf("AHIA! Il professore e\' caduto!\n");
		fflush(stdout);
		DBGpthread_mutex_lock(&mutex, ident);
		caduto++;
		DBGpthread_cond_broadcast(&wait4Fall, ident);
		printf("Arrivano il dottore e l'esorcista\n");
		fflush(stdout);
		/*aspetto che l'operazione sia conclusa da entrambi*/
		while (!esorcistaHoFatto && !medicoHoFatto) {
			DBGpthread_cond_wait(&waitOperation, &mutex, ident);
		}
		esorcistaHoFatto--;
		medicoHoFatto--;
		DBGpthread_mutex_unlock(&mutex, ident);
		DBGsleep(1, ident);
		DBGpthread_mutex_lock(&mutex, ident);
		caduto--;
		DBGpthread_mutex_unlock(&mutex, ident);
		printf("Ok, il professore si è rialzato e ora farà un po\' di lezione\n");
		fflush(stdout);
		DBGsleep(4, ident);
		printf("La lezione e\' conclusa: tutti a casa\n");
		fflush(stdout);
	}
}

int main() {
	int rc, i;
	pthread_t pth;
	intptr_t index;
	char ident[16];

	sprintf(ident, "main");

	medicoInAttesa = 0;
	esorcistaInAttesa = 0;
	caduto = 0;
	medicoHoFatto = 0;
	esorcistaHoFatto = 0;

	DBGpthread_mutex_init(&mutex, NULL, ident);
	DBGpthread_cond_init(&sync, NULL, ident);
	DBGpthread_cond_init(&wait4Fall, NULL, ident);
	DBGpthread_cond_init(&waitOperation, NULL, ident);

	for (i=0; i<NUMPROF; i++) {
		index=i;
		rc = pthread_create(&pth, NULL, Professor, (void*)index);
		if (rc) PrintERROR_andExit(errno, ident);
	}

	for (i=0; i<NUMMED; i++) {
		index=i;
		rc = pthread_create(&pth, NULL, Medic, (void*)index);
		if (rc) PrintERROR_andExit(errno, ident);
	}

	for (i=0; i<NUMEXOR; i++) {
		index=i;
		rc = pthread_create(&pth, NULL, Exorcist, (void*)index);
		if (rc) PrintERROR_andExit(errno, ident);
	}

	pthread_exit(NULL);
	return 0;
}
