/* file:  conigli.c */

/*le define necessarie sono già contenute nel makefile*/

#include "printerror.h"

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <inttypes.h>
#include <pthread.h> 

#include "DBGpthread.h"

#define CONIGLI 5
#define POSTI 2
#define CONIGLI_DA_CREARE 2

int conigliInTana, conigliInAttesa, nConigliGlobal, tanaLibera;

pthread_mutex_t mutex;
pthread_cond_t condCoda, condProcr;
pthread_attr_t attr;

void *Coniglio(void* arg) {
	char ident[32];
	pthread_t pthFiglio;
	intptr_t index = (intptr_t)arg;
	int i = index, rc;
	
	sprintf(ident, "Coniglio %d", i);
	
	DBGpthread_mutex_lock(&mutex, ident);
	/*aggiorniamo la variabile globale che mantiene il numero di conigli totali*/
	nConigliGlobal++;
	/*ora: un coniglio controlla se nella tana c'è almeno un posto libero*/
	while (conigliInTana>=POSTI && !tanaLibera) {
		/*la tana è piena*/
		conigliInAttesa++;
		DBGpthread_cond_wait(&condCoda, &mutex, ident);
		/*prima di uscire il test viene rifatto*/
		conigliInAttesa--;
	}
	
	/*c'è abbastanza posto nella tana: quanto di preciso?*/
	tanaLibera=0;
	conigliInTana++;
	printf("%s entra nella tana\n", ident); fflush(stdout);
	if (conigliInTana==1) {
		/*ancora non siamo in due (la tana era vuota). Di conseguenza devo aspettare
		 * un partner*/
		 printf("%s aspetta un partner\n", ident); fflush(stdout);
		DBGpthread_cond_wait(&condProcr, &mutex, ident);
	} else {
		/*il signal viene mandato solo dal secondo coniglio che entra
		 * nella tana*/
		DBGpthread_cond_signal(&condProcr, ident);
		printf("Avviene il sesso\n");
	}
	/*ora ci sono due conigli nella tana, entrambi svegli e pimpanti. Possono
	 * procreare*/
	 
	 DBGpthread_mutex_unlock(&mutex, ident);
	 /*l'accoppiamento è istantaneo*/
	 DBGsleep(1, ident);
	 DBGpthread_mutex_lock(&mutex, ident);
	 /*i sarà l'argomento da passare ai figli*/
	 conigliInTana--;
	 printf("%s è uscito dalla tana\n", ident); fflush(stdout);
	 /*solo il secondo che esce, lasciando la tana libera, chiamerà i conigli che aspettano, se ce ne sono*/
	 if (conigliInTana==0) {
		tanaLibera=1;
		if(conigliInAttesa>0)
			for(i=0; i<POSTI; i++)
				/*ne sveglio 2*/
				DBGpthread_cond_signal(&condCoda, ident);
	 }
	 
	 index=nConigliGlobal+1;
	 /*andiamo a morire lontano dalla mutua esclusione*/
	 DBGpthread_mutex_unlock(&mutex, ident);
	 
	 rc = pthread_create(&pthFiglio, &attr, Coniglio, (void*) index);
	 if (rc) PrintERROR_andExit(errno, "create failed");
	 
	 printf("è nato un coniglio da %s, che però è anche MORTO\n", ident); fflush(stdout);
	 
	 pthread_exit(NULL);	 
	
}

int main() {
	int rc, i;
	pthread_t pth;
	intptr_t index;
	char ident[32];
	
	sprintf(ident, "main");
	
	conigliInTana=0; nConigliGlobal=0, conigliInAttesa=0, tanaLibera=1;
	
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);
	
	rc = pthread_mutex_init(&mutex, NULL);
	if (rc) PrintERROR_andExit(errno, "Error in init");
	rc = pthread_cond_init(&condCoda, NULL);
	if (rc) PrintERROR_andExit(errno, "Error in init");
	rc = pthread_cond_init(&condProcr, NULL);
	if (rc) PrintERROR_andExit(errno, "Error in init");
	
	for(i=0; i<CONIGLI; i++) {
		index=i;
		rc = pthread_create(&pth, &attr, Coniglio, (void*) index);
		if (rc) PrintERROR_andExit(errno, "Error in create");		
	}
	
	pthread_exit(NULL);
	return 0;
}
