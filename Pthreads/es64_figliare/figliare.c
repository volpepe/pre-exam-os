#include "printerror.h"

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <inttypes.h>
#include <pthread.h> 

#include "DBGpthread.h"

#define THREAD_LIMIT 5
#define GEN_SECS 5

pthread_mutex_t mutex;

int threadnum;

void* Coniglietto(void* arg) {
	int rc; 
	pthread_t pth; 

	DBGpthread_mutex_lock(&mutex, "figlio");
	printf("Thread presenti: %d\n", threadnum); fflush(stdout);
	DBGpthread_mutex_unlock(&mutex, "figlio");

	while(1) {
		DBGpthread_mutex_lock(&mutex, "figlio");
		if (threadnum+1<=THREAD_LIMIT) {
			threadnum++;
			rc = pthread_create(&pth, NULL, Coniglietto, (void*) NULL);
			if (rc) PrintERROR_andExit(errno, "create failed");
		} else {
			threadnum--;
			DBGpthread_mutex_unlock(&mutex, "figlio");
			pthread_exit(NULL);
		}
		DBGpthread_mutex_unlock(&mutex, "figlio");
		DBGsleep(1, "figlio");
	}
}

int main(void) {
	pthread_t pth;
	int rc;

	threadnum=0;

	rc = pthread_mutex_init(&mutex, NULL);
	if (rc) PrintERROR_andExit(errno, "init failed");

	while(1) {
		DBGpthread_mutex_lock(&mutex, "main");
		rc = pthread_create(&pth, NULL, Coniglietto, (void*) NULL);
		if (rc) PrintERROR_andExit(errno, "create failed");
		threadnum++;
		DBGpthread_mutex_unlock(&mutex, "main");
		DBGsleep(GEN_SECS, "main");
	}

}



