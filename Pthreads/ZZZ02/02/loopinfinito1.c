/* loopinfinito1.c  */

#ifndef _THREAD_SAFE
#define _THREAD_SAFE
#endif

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <time.h>		/*  nanosleep() requires _POSIX_C_SOURCE >= 199309L  */

#define NUM_THREADS 50

void *do_thread(void *p_index)
{
	printf("thread index %i: thread_ID %u\n", *((int*)p_index), (unsigned int) pthread_self() );
	free(p_index);
	pthread_exit ( NULL );
}

int main()
{
	pthread_t thread;
	int rc, t, i, *p;
	struct timespec ts;
	pthread_attr_t attr;

	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);

	i=0;
	while( 1 ) {
		for( t=0; t < NUM_THREADS; t++, i++ ){

			p=malloc(sizeof(int));
			if(p==NULL) {
				perror("malloc failed: ");
				exit (1);
			}	
			*p=i;

			rc = pthread_create (&thread, &attr, do_thread, p );
			if (rc){
				printf("ERROR; return code from pthread_create() is %d\n",rc);
				exit(2);
			}
			printf("Created thread ID %u\n", (unsigned int) thread );

		}
               	ts.tv_sec=0;         /* 0 seconds */
               	ts.tv_nsec=40000000;  /* 40 milliseconds */
		nanosleep( &ts, NULL );
	}
	pthread_exit (NULL);
	return(1);
}


