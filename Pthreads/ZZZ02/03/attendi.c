/* attendi.c  
  il main crea alcuni thread, ciascun thread genera un numero casuale
  e poi attende quel numero di secondi e poi termina.
  Il main deve attendere con delle join la terminazione di tutti i thread,
  facendo per prime le join dei thread che terminano per primi le sleep
  e che quindi terminano se stessi per primi.
  Ciascun thread, dopo avere terminato la propria sleep,
  deve comunicare al main la propria intenzione di terminare,
  mettendo il proprio thread identifier in una variabile globale threadID_globale.
  Questa variabile deve essere protetta opportunamente.
  Il main, man mano che fa le join deve stampare il risultato intero
  restituito da ciascun thread, che e' proprio il numero di secondi 
  che il thread ha atteso.
*/

#ifndef _THREAD_SAFE
#define _THREAD_SAFE
#endif

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>             /* serve per la funzione random */

#define NUM_THREADS 5
pthread_t threadID_globale;

pthread_mutex_t mutex;
pthread_cond_t cond, condSync;

int mainWaits, waiting;

void *do_thread(void *p_index)
{
	int index, ris, *ptr, rc ;

	index = *((int*)p_index);
	free(p_index);
	ris = random() % 20;

	printf("thread index %i: thread_ID %u  numero casuale %i \n", index, (unsigned int) pthread_self(), ris );
	fflush(stdout);
	sleep(ris);

	printf("thread index %i: thread_ID %u  termina sleep %i \n", index, (unsigned int) pthread_self(), ris );
	fflush(stdout);

	/*aggiorno threadID_globale con il mio thread_id e poi chiamo il main*/
	rc = pthread_mutex_lock(&mutex);
	if (rc){
		printf("ERROR %d in lock\n",rc);
		exit(-1);
	}

	while (!mainWaits) {
		waiting++;
		rc = pthread_cond_wait(&condSync, &mutex);
		if (rc){
			printf("ERROR %d in wait\n",rc);
			exit(-1);
		}
		waiting--;
	}

	threadID_globale=pthread_self();
	rc = pthread_cond_signal(&cond);
	if (rc){
		printf("ERROR %d in wait\n", rc);
		exit(-1);
	}

	rc = pthread_mutex_unlock(&mutex);
	if (rc){
		printf("ERROR %d in unlock\n",rc);
		exit(-1);
	}

	/* alloco la struttura in cui restituire il risultato */
	ptr=malloc(sizeof(int));
	if(ptr==NULL) {
		perror("malloc failed: ");
		pthread_exit (NULL);
	}
	else {
		*ptr=ris;
		pthread_exit ( ptr );  /* puntatore all'intero restituito dal thread */
	}
}

int main()
{
	pthread_t th;
	int rc, t, *p;

	srandom( time(NULL) );

	rc = pthread_mutex_init(&mutex, NULL);
	if (rc) {
		printf("Error %d in init\n", rc); exit (-1);
	}
	rc = pthread_cond_init(&cond, NULL);
	if (rc) {
		printf("Error %d in init\n", rc); exit (-1);
	}
	rc = pthread_cond_init(&condSync, NULL);
	if (rc) {
		printf("Error %d in init\n", rc); exit (-1);
	}

	mainWaits=0;
	waiting=0;

	for(t=0;t < NUM_THREADS;t++){

		p=malloc(sizeof(int));
		if(p==NULL) {
			perror("malloc failed: ");
			pthread_exit (NULL);
		}
		*p=t;

		rc = pthread_create (&th, NULL, do_thread, p );
		if (rc){
			printf("ERROR; return code from pthread_create() is %d\n",rc);
			exit(-1);
		}
	}

	for(t=0;t < NUM_THREADS;t++){
		rc = pthread_mutex_lock(&mutex);
		if (rc){
			printf("ERROR %d in lock\n",rc);
			exit(-1);
		}

		if (waiting>0)
		{
			rc = pthread_cond_broadcast(&condSync);
			if (rc){
			printf("ERROR %d in broadcast\n",rc);
			exit(-1);
			}
		}

		mainWaits=1;

		rc = pthread_cond_wait(&cond, &mutex);
		if (rc){
			printf("ERROR %d in wait\n",rc);
			exit(-1);
		}
		printf("Joining %u\n", (unsigned int) pthread_self());
		rc = pthread_join(threadID_globale, (void**)&p);
		if (rc){
			printf("ERROR %d in join\n",rc);
			exit(-1);
		}

		mainWaits=0;

		rc = pthread_mutex_unlock(&mutex);
		if (rc){
			printf("ERROR %d in unlock\n",rc);
			exit(-1);
		}

		printf("Risultato restituito dal thread: %d\n", *p); fflush(stdout);
	}



	pthread_exit(NULL);
}

