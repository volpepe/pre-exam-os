/* eliminawarning01.c    eliminare i warning mantenendo il funzionamento del programma */

#ifndef _THREAD_SAFE
#define _THREAD_SAFE
#endif

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#define NUM_THREADS 50

void *do_thread(void *p_index)
{
	printf("thread index %i: thread_ID %u\n", *((int*)p_index), (unsigned int) pthread_self() );
	free(p_index);
	pthread_exit ( NULL );
}

int main()
{
	pthread_t thread;
	int rc, i, *p;
	struct timespec ts;

	for( i=0; i < NUM_THREADS; i++ ){

		p=malloc(sizeof(int));
		if(p==NULL) {
			perror("malloc failed: ");
			exit (1);
		}	
		*p=i;

		rc = pthread_create (&thread, NULL, do_thread, p );
		if (rc){
			printf("ERROR; return code from pthread_create() is %d\n",rc);
			exit(2);
		}
		printf("Created thread ID %u\n", (unsigned int) thread );

       		ts.tv_sec=0;         /* 0 seconds */
     	  	ts.tv_nsec=1000000;  /* 1 milliseconds */
		nanosleep( &ts, NULL );
	}
	
	pthread_exit (NULL);
	return(1);
}

