/*setta.c*/

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <inttypes.h>
#include <pthread.h> 

#include "printerror.h"
#include "DBGpthread.h"

#define N_NOVIZI 10
#define N_ANZIANI 6
#define NOVIZIO 0
#define ANZIANO 1

int turno, sacerdotiGiaInginocchiati;
pthread_mutex_t mutexSanto, mutexBuono, mutexStatua;
pthread_cond_t condTurno;

void *Sacerdote(void* arg) {
	intptr_t index = (intptr_t) arg;
	int i=index;
	int volteInginocchiato;
	int classe = i<N_NOVIZI? NOVIZIO : ANZIANO;
	char ident[32];
	
	sprintf(ident, "Sacerdote %s [%d]", classe==NOVIZIO? "novizio" : "anziano", i);
	volteInginocchiato=0;
	
	while(1) {
		if (classe==ANZIANO) {
			DBGpthread_mutex_lock(&mutexBuono, ident);
			printf("%s prende il vino buono\n", ident); fflush(stdout);
			DBGpthread_mutex_unlock(&mutexBuono, ident);
		}
		DBGpthread_mutex_lock(&mutexSanto, ident);
		printf("%s prende il vino santo\n", ident); fflush(stdout);
		DBGpthread_mutex_unlock(&mutexSanto, ident);
		
		DBGpthread_mutex_lock(&mutexStatua, ident);
		while (volteInginocchiato>turno) {
			DBGpthread_cond_wait(&condTurno, &mutexStatua, ident);
		}
		printf("%s si inginocchia\n", ident); fflush(stdout);
		volteInginocchiato++;
		sacerdotiGiaInginocchiati++;
		if (sacerdotiGiaInginocchiati%(N_ANZIANI+N_NOVIZI)==0) {
			printf("tutti si sono inginocchiati %d volte\n", ++turno);
			DBGpthread_cond_broadcast(&condTurno, ident);
		}
		DBGpthread_mutex_unlock(&mutexStatua, ident);
	}
	pthread_exit(NULL);
}

int main() {
	int rc, i;
	intptr_t index;
	pthread_t th;
	
	turno=0; sacerdotiGiaInginocchiati=0;
	
	rc = pthread_cond_init(&condTurno, NULL);
	if( rc ) PrintERROR_andExit(errno,"pthread_cond_init failed");
	rc = pthread_mutex_init(&mutexSanto, NULL); 
	if( rc ) PrintERROR_andExit(errno,"pthread_mutex_init failed");
	rc = pthread_mutex_init(&mutexBuono, NULL); 
	if( rc ) PrintERROR_andExit(errno,"pthread_mutex_init failed");
	rc = pthread_mutex_init(&mutexStatua, NULL); 
	if( rc ) PrintERROR_andExit(errno,"pthread_mutex_init failed");

	for(i=0;i<N_NOVIZI+N_ANZIANI;i++) {
		index=i;
		rc=pthread_create(&th,NULL, Sacerdote,(void*)index); 
		if(rc) PrintERROR_andExit(errno,"pthread_create failed");
	}
	
	pthread_exit(NULL);
	return(0); 
	
}
