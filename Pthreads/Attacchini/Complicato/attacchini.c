/*attacchini.c*/

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>		/* uint64_t */
#include <sys/time.h>	/* gettimeofday()    struct timeval */
#include <pthread.h> 

#include "printerror.h"
#include "DBGpthread.h"

#define N_VECCHIETTI 4
#define N_ATTACCHINI 2

int lavoriInCorso, vecchiettiInLettura, bacheca;

pthread_mutex_t mutexBacheca, mutexLavoro;
pthread_cond_t condInizioLav, condLavoro;

void *Vecchietto(void* arg) {
	intptr_t index = (intptr_t)arg;
	int i = index;
	char ident[32];
	
	sprintf(ident, "Vecchietto %d", i);
	
	while(1) {
		DBGpthread_mutex_lock(&mutexLavoro, ident);
		/*controlliamo subito se gli attacchini stanno occupando la bacheca*/
		while(lavoriInCorso) {
			printf("%s aspetta di poter leggere\n", ident); fflush(stdout);
			DBGpthread_cond_wait(&condLavoro, &mutexLavoro, ident);
		}
		/*se arriviamo qua vuol dire che non ci sono attacchini alla bacheca*/
		/*se sono il primo vecchietto prendo il lock sulla bacheca, altrimenti ce 
		 * l'ho già e posso proseguire*/
		if (vecchiettiInLettura==0) {
			DBGpthread_mutex_lock(&mutexBacheca, ident);
		}
		vecchiettiInLettura++;
		DBGpthread_mutex_unlock(&mutexLavoro, ident);
		/*non rilascio il lock su mutexBacheca*/
		printf("%s legge dalla bacheca: oh no %d è proprio morto\n", ident, bacheca); fflush(stdout);
		DBGsleep(3, ident);
		printf("%s ha smesso di leggere\n", ident); fflush(stdout);
		DBGpthread_mutex_lock(&mutexLavoro, ident);
		/*ho finito di leggere*/
		vecchiettiInLettura--;
		/*se sono l'ultimo vecchietto lascio libera la bacheca sbloccando la mutex*/
		if (vecchiettiInLettura==0) {
			DBGpthread_mutex_unlock(&mutexBacheca, ident); 
		}
		DBGpthread_mutex_unlock(&mutexLavoro, ident);
		DBGsleep(1+(i%2), ident);
	}
	pthread_exit(NULL);
}

void *Attacchino(void* arg) {
	intptr_t index = (intptr_t)arg;
	int i = index;
	char ident[32];
	
	sprintf(ident, "Attacchino %d", i);
	
	while(1) {
		DBGpthread_mutex_lock(&mutexLavoro, ident);
		/*mi avvicino subito alla bacheca. se sono il primo attacchino
		 * attivo lavori in corso così nel frattempo i vecchietti sgomberano
		 * e inoltre così potrò farmi svegliare dall'altro attacchino che arriva*/
		if(!lavoriInCorso) {
			printf("Gli attacchini vogliono lavorare\n"); fflush(stdout);
			lavoriInCorso++;
			DBGpthread_cond_wait(&condInizioLav, &mutexLavoro, ident);
		} else {
			/*qua entra il secondo attacchino e sveglia il primo*/
			DBGpthread_cond_signal(&condInizioLav, ident);
			printf("Gli attacchini si mettono a lavorare\n"); fflush(stdout);
		}
		DBGpthread_mutex_unlock(&mutexLavoro, ident);
		/*rilascio mutexLavoro perchè nel frattempo devo permettere ai
		* vecchietti di andare via*/
		DBGpthread_mutex_lock(&mutexBacheca, ident);
		bacheca++;
		DBGpthread_mutex_unlock(&mutexBacheca, ident);
		
		DBGsleep(2, ident);
		
		DBGpthread_mutex_lock(&mutexLavoro, ident);
		if (lavoriInCorso) {
			/*solo uno dei due fa tutte le modifiche così non le rifacciamo due volte*/
			printf("Gli attacchini hanno finito di lavorare\n"); fflush(stdout);
			lavoriInCorso--;
			DBGpthread_cond_broadcast(&condLavoro, ident);
		}
		DBGpthread_mutex_unlock(&mutexLavoro, ident);
		DBGsleep(2, ident);
	}
	pthread_exit(NULL);
}

int main() {
	int rc, i;
	intptr_t index;
	pthread_t pth;
	
	lavoriInCorso=0; vecchiettiInLettura=0, bacheca=0;
	
	rc = pthread_mutex_init(&mutexBacheca, NULL);
	if (rc) PrintERROR_andExit(errno, "errore in init");
	rc = pthread_mutex_init(&mutexLavoro, NULL);
	if (rc) PrintERROR_andExit(errno, "errore in init");
	rc = pthread_cond_init(&condInizioLav, NULL);
	if (rc) PrintERROR_andExit(errno, "errore in init");
	rc = pthread_cond_init(&condLavoro, NULL);
	if (rc) PrintERROR_andExit(errno, "errore in init");
	
	for (i=0; i<N_VECCHIETTI; i++) {
		index=i;
		rc = pthread_create(&pth, NULL, Vecchietto, (void*) index);
		if (rc) PrintERROR_andExit(errno, "errore in pthread_create");
	}
	
	for (i=0; i<N_ATTACCHINI; i++) {
		index=i;
		rc = pthread_create(&pth, NULL, Attacchino, (void*) index);
		if (rc) PrintERROR_andExit(errno, "errore in pthread_create");
	}
	
	pthread_exit(NULL);
	return 0;
}
