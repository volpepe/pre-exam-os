/* file:  attacchini.c */

/* messo prima perche' contiene define _POSIX_C_SOURCE */
#include "printerror.h"
#include "DBGpthread.h"

#include <unistd.h>   /* exit() etc */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>     /* per strerror_r  and  memset */
#include <sys/mman.h>  /* shm_* stuff, and mmap() */
#include <sys/types.h>
#include <signal.h>
#include <sys/time.h>	/* timeval{} for select() */
#include <time.h>	/* timespec{} for pselect() */
#include <limits.h>	/* for OPEN_MAX */
#include <errno.h>
#include <sys/wait.h>
#include <assert.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdint.h>
#include <inttypes.h>

#include <fcntl.h>
#include <sys/stat.h>

#define NUMLAV 2
#define NUMSEC 5

typedef struct {
	int arrivati;
	pthread_mutex_t mutex;
	pthread_cond_t condInizioLav, condOrologio, condFineLav;
} Lavoro;

Lavoro* l;

void Attacchino(int i) {
	char ident[32];
	
	sprintf(ident, "Attacchino %d", i);
	
	while(1) {
		DBGpthread_mutex_lock(&l->mutex, ident);
		l->arrivati++;
		/*qui si entra solo se ci sono due attacchini: si può lavorare*/
		/*svegliamo l'altro attacchino e l'orologio (solo se siamo i secondi)*/
		if (l->arrivati==NUMLAV) {
			printf("I due attacchini sono pronti per lavorare\n"); fflush(stdout);
			DBGpthread_cond_signal(&l->condInizioLav, ident);
			DBGpthread_cond_signal(&l->condOrologio, ident);
		}
		else {
			printf("Arriva un solo attacchino...\n"); fflush(stdout);
			DBGpthread_cond_wait(&l->condInizioLav, &l->mutex, ident);
		}
		/*entrambi aspettano la fine dei lavori*/
		DBGpthread_cond_wait(&l->condFineLav, &l->mutex, ident);
		/*una volta finiti i lavori gli attacchini rilasciano la mutex e 
		 * tornano subito. Reimpostiamo la variabile "arrivati" a 0 solo la
		 * prima volta*/
		 if (l->arrivati==NUMLAV) { l->arrivati=0; }
		 DBGpthread_mutex_unlock(&l->mutex, ident);		
		 /*impostandola direttamente a 0 evitiamo che il primo thread che 
		  * rientra nel ciclo possa saltare il wait iniziale senza aspettare 
		  * l'altro*/
	}
	exit(0);	 
}

void Orologio(int tempo) {
	char ident[32];
	
	sprintf(ident, "Orologio");
	while (1) {
		DBGpthread_mutex_lock(&l->mutex, ident);
		/*aspettiamo di essere chiamati dal secondo lavoratore*/
		DBGpthread_cond_wait(&l->condOrologio, &l->mutex, ident);
		/*quando veniamo chiamati iniziamo a contare tempo secondi*/
		DBGpthread_mutex_unlock(&l->mutex, ident);
		printf("Si comincia a lavorare!\n"); fflush(stdout);
		DBGsleep(5, ident);
		printf("Lavoro terminato!\n"); fflush(stdout);
		DBGpthread_mutex_lock(&l->mutex, ident);
		/*chiamiamo entrambi i lavoratori*/
		DBGpthread_cond_broadcast(&l->condFineLav, ident);
		DBGpthread_mutex_unlock(&l->mutex, ident);
		/*ricominciamo*/
	}
}

int main() {
	int shmfd, rc, i;
	pid_t pid;
	int shared_seg_size = sizeof(Lavoro);
	pthread_mutexattr_t mattr;
	pthread_condattr_t cvattr;
	
	/*creiamo il segmento di memoria condivisa con il nome /attacchini*/
	shmfd=shm_open( "/attacchini", O_CREAT | O_RDWR, S_IRWXU );
	if (shmfd < 0) PrintERROR_andExit(errno ,"Errore in shm_open()");
	
	/*con ftruncate impostiamo la dimensione del segmento in memoria*/
	rc = ftruncate(shmfd, shared_seg_size);
	if (rc!=0) PrintERROR_andExit(rc ,"Errore in ftruncate");

	/*infine con mmap portiamo il segmento in memoria nel nostro spazio di indirizzamento
	 * così da poterlo usare all'interno del nostro programma*/
	l=(Lavoro*)mmap(NULL, shared_seg_size,
		PROT_READ|PROT_WRITE, MAP_SHARED, shmfd, 0);
	if( l==MAP_FAILED ) PrintERROR_andExit(errno,"Errore in mmap");
	
	/*ora possiamo inizializzare i campi della struttura*/
	l->arrivati=0;
	
	/*inizializziamo anche le varie cond e mutex*/
	rc=pthread_mutexattr_init(&mattr);
	if( rc ) PrintERROR_andExit(errno,"pthread_mutexattr_init failed");
	rc=pthread_mutexattr_setpshared(&mattr,PTHREAD_PROCESS_SHARED);
	if( rc ) PrintERROR_andExit(errno,"pthread_mutexattr_setpshared failed");
	rc=pthread_condattr_init(&cvattr);
	if( rc ) PrintERROR_andExit(errno,"pthread_condattr_init failed");
	rc=pthread_condattr_setpshared(&cvattr,PTHREAD_PROCESS_SHARED);
	if( rc ) PrintERROR_andExit(errno,"pthread_condattr_setpshared failed");
	
	rc = pthread_cond_init(&l->condFineLav, &cvattr);
	if( rc ) PrintERROR_andExit(errno,"pthread_cond_init failed");
	rc = pthread_cond_init(&l->condInizioLav, &cvattr);
	if( rc ) PrintERROR_andExit(errno,"pthread_cond_init failed");
	rc = pthread_cond_init(&l->condOrologio, &cvattr);
	if( rc ) PrintERROR_andExit(errno,"pthread_cond_init failed");
	rc = pthread_mutex_init(&l->mutex, &mattr); 
	if( rc ) PrintERROR_andExit(errno,"pthread_mutex_init failed");
	
	/*da QUI in poi si creano FINALMENTE i processi*/
	for (i=0; i<NUMLAV; i++) {
		pid=fork();
		if(pid<0) PrintERROR_andExit(errno,"fork failed");
		else if (pid==0) {
			/*questo è il figlio*/
			Attacchino(i);
			exit(0);
		}
		/*il padre non fa nulla perchè semplicemente deve ripetersi il for 2 volte e poi 
		 * bisogna uscire*/
	}
	Orologio(NUMSEC);
	
	return 0;	
}
