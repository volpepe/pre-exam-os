/*staffetta.c*/

/*i define importanti sono già contenuti all'interno del makefile*/

#include "printerror.h"

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <pthread.h>

#include "DBGpthread.h"

#define SQUADRE 2
#define ALFA 0
#define BETA 1
#define N_ALFA 5
#define N_BETA 4

pthread_mutex_t mutex;
pthread_cond_t condFila[SQUADRE];

int turno[SQUADRE], faiGiro[SQUADRE], atletiGiaFatto[SQUADRE], pistaOccupata;
int atletiInAttesa[SQUADRE];

void* Atleta(void* arg) {
	intptr_t index = (intptr_t) arg;
	int i = index;
	char ident[32];
	int giro=0;
	int squadra = i<N_ALFA? ALFA : BETA;
	
	sprintf(ident, "atleta [%d], squadra %s", i, squadra==ALFA? "alfa" : "beta");
	
	while(1) {
		DBGpthread_mutex_lock(&mutex, ident);
		/*un atleta può partire se è della squadra che può partire e
		 * se la sua variabile giro ha lo stesso valore di turno e se
		 * non c'è nessun altro in pista*/
		while (!faiGiro[squadra] || giro!=turno[squadra] || pistaOccupata) {
			atletiInAttesa[squadra]++;
			DBGpthread_cond_wait(&condFila[squadra], &mutex, ident);
			atletiInAttesa[squadra]--;
		}
		
		printf("parte %s\n", ident); fflush(stdout);
		
		/*se usciamo è perchè possiamo correre*/
		/*ogni atleta impiega un secondo per fare un giro*/
		pistaOccupata=1;
		DBGpthread_mutex_unlock(&mutex, ident);
		DBGsleep(1, ident);
		DBGpthread_mutex_lock(&mutex, ident);
		/*sistemiamo tutte le variabili*/
		printf("%s ha sgomberato la pista\n", ident); fflush(stdout);
		pistaOccupata=0;
		giro++;
		atletiGiaFatto[squadra]++;
		/*ora gli altri devono girare*/
		faiGiro[(squadra+1)%SQUADRE]=1;
		faiGiro[squadra]=0;
		/*se siamo l'ultimo atleta della nostra squadra che ha fatto l'n-esimo giro
		 * aumentiamo di 1 la variabile che indica il turno*/
		if (atletiGiaFatto[squadra]==(squadra==ALFA? N_ALFA : N_BETA)) {
			atletiGiaFatto[squadra]=0;
			turno[squadra]++;
		}
		/*segnaliamo qualcuno della squadra opposta*/
		if (atletiInAttesa[(squadra+1)%SQUADRE]>0) {
			DBGpthread_cond_broadcast(&condFila[(squadra+1)%SQUADRE], ident);
		}
		DBGpthread_mutex_unlock(&mutex, ident);
	}
	 
}

int main() {
	int rc, i;
	intptr_t index;
	pthread_t pth;
	
	turno[ALFA]=0; turno[BETA]=0;
	faiGiro[ALFA]=1; faiGiro[BETA]=0;
	atletiGiaFatto[ALFA]=0; atletiGiaFatto[BETA]=0;
	pistaOccupata=0;
	atletiInAttesa[ALFA]=0; atletiInAttesa[BETA]=0;
	/*parte alfa*/
	
	rc = pthread_mutex_init(&mutex, NULL);
	if (rc) PrintERROR_andExit(errno, "error in init");
	rc = pthread_cond_init(&condFila[ALFA], NULL);
	if (rc) PrintERROR_andExit(errno, "error in init");
	rc = pthread_cond_init(&condFila[BETA], NULL);
	if (rc) PrintERROR_andExit(errno, "error in init");
	
	for (i=0; i<N_ALFA+N_BETA; i++) {
		index=i;
		rc = pthread_create(&pth, NULL, Atleta, (void*)index);
		if (rc) PrintERROR_andExit(errno, "error in pthread_create");
	}
	
	pthread_exit(NULL);
	return 0;
}
