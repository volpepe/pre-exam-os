/*staffetta_con_processi.c*/

/*i define importanti sono già contenuti all'interno del makefile*/

#include "printerror.h"
#include "DBGpthread.h"

#include <unistd.h>   	/* exit() etc */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>     /* per strerror_r  and  memset */
#include <sys/mman.h> 	/* shm_* stuff, and mmap() */
#include <sys/types.h>
#include <signal.h>
#include <sys/time.h>	/* timeval{} for select() */
#include <time.h>		/* timespec{} for pselect() */
#include <limits.h>		/* for OPEN_MAX */
#include <errno.h>
#include <sys/wait.h>
#include <assert.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdint.h>
#include <inttypes.h>

#include <fcntl.h>
#include <sys/stat.h>

#define SQUADRE 2
#define ALFA 0
#define BETA 1
#define N_ALFA 5
#define N_BETA 4

typedef struct {
	pthread_mutex_t mutex;
	pthread_cond_t condFila[SQUADRE];
	int turno[SQUADRE], faiGiro[SQUADRE], atletiGiaFatto[SQUADRE], pistaOccupata;
	int atletiInAttesa[SQUADRE];
} Campo;

Campo* c;

void* Atleta(int i, int squadra) {
	char ident[32];
	int giro=0;
	
	sprintf(ident, "atleta [%d], squadra %s", i, squadra==ALFA? "alfa" : "beta");
	
	while(1) {
		DBGpthread_mutex_lock(&c->mutex, ident);
		/*un atleta può partire se è della squadra che può partire e
		 * se la sua variabile giro ha lo stesso valore di turno e se
		 * non c'è nessun altro in pista*/
		while (!c->faiGiro[squadra] || giro!=c->turno[squadra] || c->pistaOccupata) {
			c->atletiInAttesa[squadra]++;
			DBGpthread_cond_wait(&c->condFila[squadra], &c->mutex, ident);
			c->atletiInAttesa[squadra]--;
			if (!c->faiGiro[squadra] || giro!=c->turno[squadra] || c->pistaOccupata)
			DBGpthread_cond_signal(&c->condFila[(squadra+1)%SQUADRE], ident);
		}
		
		printf("parte %s\n", ident); fflush(stdout);
		
		/*se usciamo è perchè possiamo correre*/
		/*ogni atleta impiega un secondo per fare un giro*/
		c->pistaOccupata=1;
		DBGpthread_mutex_unlock(&c->mutex, ident);
		DBGsleep(1, ident);
		DBGpthread_mutex_lock(&c->mutex, ident);
		/*sistemiamo tutte le variabili*/
		printf("%s ha sgomberato la pista\n", ident); fflush(stdout);
		c->pistaOccupata=0;
		giro++;
		c->atletiGiaFatto[squadra]++;
		/*ora gli altri devono girare*/
		c->faiGiro[(squadra+1)%SQUADRE]=1;
		c->faiGiro[squadra]=0;
		/*se siamo l'ultimo atleta della nostra squadra che ha fatto l'n-esimo giro
		 * aumentiamo di 1 la variabile che indica il turno*/
		if (c->atletiGiaFatto[squadra]==(squadra==ALFA? N_ALFA : N_BETA)) {
			c->atletiGiaFatto[squadra]=0;
			c->turno[squadra]++;
		}
		/*segnaliamo qualcuno della squadra opposta*/
		if (c->atletiInAttesa[(squadra+1)%SQUADRE]>0) {
			DBGpthread_cond_signal(&c->condFila[(squadra+1)%SQUADRE], ident);
		}
		DBGpthread_mutex_unlock(&c->mutex, ident);
	}
	pthread_exit(NULL);
}

int main() {
	int shmfd, rc, i;
	pid_t pid;
	int shared_seg_size = sizeof(Campo);
	pthread_mutexattr_t mattr;
	pthread_condattr_t cvattr;

	/*Si crea un segmento di memoria condivisa e lo si associa al nome /staffetta*/
	shmfd = shm_open( "/staffetta", O_CREAT | O_RDWR, S_IRWXU );
	if (shmfd < 0) PrintERROR_andExit(errno,"errore in shm_open()");
	
	/*impostiamo la dimensione di /staffetta, che ha come file descriptor shmfd*/
	rc = ftruncate(shmfd, shared_seg_size);
	if (rc != 0) PrintERROR_andExit(errno,"errore in ftruncate()");

	/*facciamo puntare il puntatore globale c al segmento di memoria*/
	c = (Campo*)mmap(NULL, shared_seg_size,
		PROT_READ | PROT_WRITE, MAP_SHARED, shmfd, 0);
	if( c==MAP_FAILED )
	 		PrintERROR_andExit(errno,"Errore in mmap()");
	
	/*inizializziamo tutti i campi della struttura dati*/
	c->turno[ALFA]=0; c->turno[BETA]=0;
	c->faiGiro[ALFA]=1; c->faiGiro[BETA]=0;
	c->atletiGiaFatto[ALFA]=0; c->atletiGiaFatto[BETA]=0;
	c->pistaOccupata=0;
	c->atletiInAttesa[ALFA]=0; c->atletiInAttesa[BETA]=0;
	/*parte alfa*/

	rc=pthread_mutexattr_init(&mattr);
	if( rc ) PrintERROR_andExit(errno,"Errore in pthread_mutexattr_init()");
	rc=pthread_mutexattr_setpshared(&mattr,PTHREAD_PROCESS_SHARED);
	if( rc ) PrintERROR_andExit(errno,"Errore in pthread_mutexattr_setpshared()");
	
	rc=pthread_condattr_init(&cvattr);
	if( rc ) PrintERROR_andExit(errno,"Errore in pthread_condattr_init()");
	rc=pthread_condattr_setpshared(&cvattr,PTHREAD_PROCESS_SHARED);
	if( rc ) PrintERROR_andExit(errno,"Errore in pthread_condattr_setpshared()");

	if( rc ) PrintERROR_andExit(errno,"Errore in pthread_cond_init()");
	rc = pthread_cond_init(&c->condFila[BETA], &cvattr);
	if( rc ) PrintERROR_andExit(errno,"Errore in pthread_cond_init()");

	rc = pthread_mutex_init(&c->mutex, &mattr); 
	if( rc ) PrintERROR_andExit(errno,"Errore in pthread_mutex_init()");


	for(i=0; i<N_ALFA+N_BETA; i++) {
		pid=fork();
		if(pid < 0) 
			PrintERROR_andExit(errno,"Errore in fork()");
		else if(pid==0) {
			/*figli*/
			if( i<N_ALFA )
				Atleta(i, ALFA);
			else 
				Atleta(i, BETA);
			exit(0);
		}
	}

	return(0); 
}

