/*file: guadoZambesi.c */

/*THREAD_SAFE and C_POSIX_SOURCE are already defined by the makefile*/

#include "printerror.h"

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <inttypes.h>
#include <pthread.h> 

#include "DBGpthread.h"

#define N_IPPO 2
#define N_LEMU 7 

pthread_cond_t condQueueHippo, condWaitHippo, condWaitSide;
pthread_mutex_t mutex;

int freeWaters, ready, onBoard;

void* Hippo(void* arg) {
	intptr_t index = (intptr_t) arg;
	char ident[32];

	sprintf(ident, "Hippo % " PRIiPTR "", index);

	while (1) {
		DBGpthread_mutex_lock(&mutex, ident);
		/*checking if the water is free*/
		if (!freeWaters) {
			/*if the water is not free we can wait on queueHippo*/
			DBGpthread_cond_wait(&condQueueHippo, &mutex, ident);
		}

		/*if we arrive here the water is free and we can start our descent*/
		/*but first: we need to claim the water*/
		printf("%s is descending into the waters\n", ident); fflush(stdout);
		freeWaters=0;
		ready=1;
		/*and tell the lemurs too*/
		DBGpthread_cond_signal(&condWaitSide, ident);
		DBGpthread_mutex_unlock(&mutex, ident);

		/*we wait out of the lock*/
		DBGsleep(2, ident);

		DBGpthread_mutex_lock(&mutex, ident);
		printf("%s is carrying %d lemurs\n", ident, onBoard); fflush(stdout);
		ready=0;
		DBGpthread_mutex_unlock(&mutex, ident);

		/*crossing the river*/
		DBGsleep(3, ident);

		DBGpthread_mutex_lock(&mutex, ident);
		printf("%s has arrived on the other side\n", ident); fflush(stdout);
		/*waters are now free: we can call the other hippo as well*/
		/*I'll wake up the lemurs on my back*/
		DBGpthread_cond_broadcast(&condWaitHippo, ident);
		freeWaters=1;
		DBGpthread_cond_signal(&condQueueHippo, ident);
		printf("%s gets caught in the tornado!\n", ident); fflush(stdout);
		DBGpthread_mutex_unlock(&mutex, ident);

		/*the tornado does its thing*/
		DBGsleep(3, ident);
		
		DBGpthread_mutex_lock(&mutex, ident);
		printf("%s is back on the north side\n", ident); fflush(stdout);
		DBGpthread_mutex_unlock(&mutex, ident);
	}
}

void *Lemu(void* arg) {
	intptr_t index = (intptr_t) arg;
	char ident[32];

	sprintf(ident, "Lemur % " PRIiPTR "", index);

	while (1) {
		DBGpthread_mutex_lock(&mutex, ident);

		/*in order to get on the hippo we need to check if:
		-there is a ready hippo
		-the ready hippo has less than 4 lemurs on his back.*/
		DBGpthread_cond_wait(&condWaitSide, &mutex, ident);
		if (ready) {
			onBoard++;
			printf("%s hops on the hippo's back\n", ident); fflush(stdout);
			/*1, 2 and 3 signal other lemurs, 4 doesn't so everyone just waits*/
			if (onBoard<4) DBGpthread_cond_signal(&condWaitSide, ident);
			DBGpthread_cond_wait(&condWaitHippo, &mutex, ident);

			/*when we get signalled we have to hop off the hippo's back*/
			onBoard--;
			printf("%s hops off the hippo's back\n", ident); fflush(stdout);
			if (onBoard>0) DBGpthread_cond_signal(&condWaitHippo, ident);

			/*now the tornado comes by: we need to wait three secs*/
			printf("%s gets caught in the tornado!\n", ident); fflush(stdout);
			DBGpthread_mutex_unlock(&mutex, ident);

			/*the tornado does its thing*/
			DBGsleep(3, ident);
		
			DBGpthread_mutex_lock(&mutex, ident);
			printf("%s is back on the north side\n", ident); fflush(stdout);
		}
		DBGpthread_mutex_unlock(&mutex, ident);
	}
}

int main(void) {
	int rc, i;
	intptr_t index;
	char ident[32];
	pthread_t pth;

	sprintf(ident, "main");

	freeWaters=1; ready=0; onBoard=0;

	rc=pthread_mutex_init(&mutex, NULL);
	if (rc) PrintERROR_andExit(errno, ident);
	rc=pthread_cond_init(&condQueueHippo, NULL);
	if (rc) PrintERROR_andExit(errno, ident);
	rc=pthread_cond_init(&condWaitHippo, NULL);
	if (rc) PrintERROR_andExit(errno, ident);
	rc=pthread_cond_init(&condWaitSide, NULL);
	if (rc) PrintERROR_andExit(errno, ident);

	for(i=0; i<N_IPPO; i++) {
		index=i;
		rc = pthread_create(&pth, NULL, Hippo, (void*) index);
		if (rc) PrintERROR_andExit(rc, ident);
	}

	for(i=0; i<N_LEMU; i++) {
		index=i;
		rc = pthread_create(&pth, NULL, Lemu, (void*) index);
		if (rc) PrintERROR_andExit(rc, ident);
	}

	pthread_exit(NULL);
	return 0;
}


