/*ex 33: nprodmcons_incategorie.c*/

#ifndef _THREAD_SAFE
	#define _THREAD_SAFE
#endif
#ifndef _POSIX_C_SOURCE
	#define _POSIX_C_SOURCE 200112L
#endif

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <inttypes.h>
#include <pthread.h> 

#include "printerror.h"
#include "DBGpthread.h"

#define NPROD 10
#define NCONS_A 3
#define NCONS_B 4
#define A 0
#define B 1

long int buffer; /*to protect*/
int buffFull = 0, lastCons = B; /*when the program starts lastCons is  set to
								B so that the first consumer will be an A
								consumer*/
int ready;

pthread_mutex_t mutex;
pthread_cond_t condProd, condCons[2]; 

void *Cons (void* arg) {
	intptr_t i = (intptr_t) arg;
	long int caught;
	char ident[32];
	int type = i%2==1 ? A : B;

	sprintf(ident, "Consumer %c: %" PRIiPTR "", type==A? 'A' : 'B', i);

	while (1) {
		DBGpthread_mutex_lock(&mutex, ident);
		/*can we consume whatevers inside the buffer? only if the buffer is full
		and if we are consumers of the correct type*/
		if (buffFull==0 || ready==0 || lastCons==type) {
			DBGpthread_cond_wait(&condCons[type], &mutex, ident);
			/*we are sure that whoever gets signalled here is of the right
			type and that the buffer is ready to be analyzed*/
		}

		DBGpthread_mutex_unlock(&mutex, ident);
		/*let's grab the product*/
		DBGsleep(1, ident);
		DBGpthread_mutex_lock(&mutex, ident);
		caught=buffer;

		printf("%s has withdrawn %ld from the buffer\n", ident, caught); fflush(stdout);

		/*now that we have the product we must reset the buffer*/
		buffFull=0; 
		lastCons=type;
		DBGpthread_cond_signal(&condProd, ident);
		DBGpthread_mutex_unlock(&mutex, ident);

		/*consume the product*/
		DBGsleep(1, ident);
	}
}

void *Prod (void* arg) {
	intptr_t i = (intptr_t)arg;
	long int product=0;
	char ident[32];

	sprintf(ident, "Producer %" PRIiPTR "", i);

	while (1) {
		/*producer creates the product*/
		DBGsleep(1, ident);
		product++;

		/*can we deposit our product? only if the buffer is empty*/
		DBGpthread_mutex_lock(&mutex, ident);

		while (buffFull!=0) {
			/*if the buffer is in use, we shall wait*/
			DBGpthread_cond_wait(&condProd, &mutex, ident);
			/*when we wake up we can use the buffer.
			we re-check with a while because of this situation:
			during the unlock in the sleep below, if one of the threads
			waiting here has been signalled it will resume its job:
			so there will be two producers storing their products at the
			same time (and two consumers signalled etc etc)*/
		}

		/*from now on the buffer is empty*/
		ready=0;
		buffFull=1; 
		buffer=product; /*we deposit our product in the buffer: it takes 1 second*/
		DBGpthread_mutex_unlock(&mutex, ident);
		/*waiting must be done out of a mutex variable: the buffer is still in
		use anyway*/
		DBGsleep(1, ident);
		DBGpthread_mutex_lock(&mutex, ident);

		printf("%s has deposited %ld\n", ident, product); fflush(stdout);
		/*the product has been deposited: we must signal a consumer. A or B?*/
		lastCons==A? DBGpthread_cond_signal(&condCons[B], ident) :	
					 DBGpthread_cond_signal(&condCons[A], ident);

		/*we have deposited the product and signalled a consumer: now we are ready
		to release the mutual exclusion. Buffer is still full.*/
		ready=1;
		DBGpthread_mutex_unlock(&mutex, ident);
	}	
}

int main (void) {
	char ident[32];
	int rc, i;
	pthread_t pth;
	intptr_t index;

	/*initialize posix structures*/
	sprintf(ident, "main");
	rc = pthread_mutex_init(&mutex, NULL);
	if ( rc ) PrintERROR_andExit(rc, ident);
	rc = pthread_cond_init(&condProd, NULL);
	if ( rc ) PrintERROR_andExit(rc, ident);
	rc = pthread_cond_init(&condCons[A], NULL);
	if ( rc ) PrintERROR_andExit(rc, ident);
	rc = pthread_cond_init(&condCons[B], NULL);
	if ( rc ) PrintERROR_andExit(rc, ident);

	for (i=0; i<NPROD; i++) {
		index=i;
		rc = pthread_create( &pth, NULL, Prod, (void*) index );
		if ( rc ) PrintERROR_andExit(rc, ident);
	}

	for (i=0; i<( NCONS_A + NCONS_B ); i++) {
		index=i;
		rc = pthread_create( &pth, NULL, Cons, (void*) index );
		if ( rc ) PrintERROR_andExit(rc, ident);
	}

	pthread_exit(NULL);
	return 0;
}