/* file:  alieni.c */

/*i define importanti sono già contenuti nel makefile*/

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <inttypes.h>
#include <pthread.h> 

#include "printerror.h"
#include "DBGpthread.h"

#define POSTI 2
#define ALIENI_START 5
#define CONTROLLO 3
#define ALIENI_CREATI 2

pthread_mutex_t mutex;
pthread_cond_t condFila, condCasa;

pthread_attr_t attr;

int alieniInFila, alieniInCasa, casaLibera;

void *Alieno(void* arg) {
	intptr_t index = (intptr_t) arg;
	int rc, i=index;
	pthread_t pth;
	char ident[32];
	
	sprintf(ident, "Alieno [%d]", i);
	
	while(1) {
		DBGpthread_mutex_lock(&mutex, ident);
		
		if (alieniInCasa>=POSTI || !casaLibera) {
			alieniInFila++;
			DBGpthread_cond_wait(&condFila, &mutex, ident);
			alieniInFila--;
		}
		
		/*entro in casa*/
		alieniInCasa++;
		printf("%s entra in casa\n", ident); fflush(stdout);
		if (alieniInCasa<POSTI) {
			/*sono il primo*/
			DBGpthread_cond_wait(&condCasa, &mutex, ident);
		} else {
			/*sono il secondo*/
			casaLibera=0;
			DBGpthread_cond_signal(&condCasa, ident);
			DBGpthread_cond_wait(&condCasa, &mutex, ident);
		}
		
		alieniInCasa--;		
		printf("%s esce di casa\n", ident); fflush(stdout);
		/*il primo di quelli arrivati e attualmente presenti in casa esce*/
		/*appena uscito chiamo qualcuno in fila (se c'è qualcuno)*/
		if (alieniInFila>0) DBGpthread_cond_signal(&condFila, ident);
		else casaLibera=1;
		
		/*controlla quanti alieni sono in fila*/
		if (alieniInFila<CONTROLLO) {
			printf("%s crea due figli\n", ident); fflush(stdout);
			for (i=0; i<ALIENI_CREATI; i++) {
				index=index+i+1;
				rc = pthread_create(&pth, &attr, Alieno, (void*) index);
				if (rc) PrintERROR_andExit(errno, "error in create");
			}
		}
			
		printf("%s muore\n", ident); fflush(stdout);
		DBGpthread_mutex_unlock(&mutex, ident);	
		/*adesso muoio*/
		pthread_exit(NULL);
		
	}
}

int main() {
	int rc, i;
	intptr_t index;
	pthread_t pth;
	
	alieniInCasa=0; alieniInFila=0, casaLibera=1;
	
	rc = pthread_mutex_init(&mutex, NULL);
	if (rc) PrintERROR_andExit(errno, "error in init");
	rc = pthread_cond_init(&condCasa, NULL);
	if (rc) PrintERROR_andExit(errno, "error in init");
	rc = pthread_cond_init(&condFila, NULL);
	if (rc) PrintERROR_andExit(errno, "error in init");
	
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);
	
	for (i=0; i<ALIENI_START; i++) {
		index = i;
		rc = pthread_create(&pth, &attr, Alieno, (void*) index);
		if (rc) PrintERROR_andExit(errno, "error in create");
	}
	
	pthread_exit(NULL);
	return 0;
}
