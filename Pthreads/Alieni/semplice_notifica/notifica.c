/*notifica.c*/

/*le define necessarie sono già presenti all'interno del makefile*/

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <inttypes.h>
#include <pthread.h> 

#include "printerror.h"
#include "DBGpthread.h"

#define INIZ 10

pthread_t daJoinare;
int join, indiceGlob;
pthread_mutex_t mutex;
pthread_cond_t condJoin, condAspettaJoin;

void *Notifica(void* arg) {
	pthread_t pth;
	intptr_t index = (intptr_t) arg;
	int i = index, rc, *ptr;
	char ident[32];
	
	sprintf(ident, "Notifica [%d]", i);
	
	DBGsleep(1, ident);
	
	DBGpthread_mutex_lock(&mutex, ident);
	
	while(join) {
		DBGpthread_cond_wait(&condAspettaJoin, &mutex, ident);
	}
	
	daJoinare=pthread_self();
	join=1;
	DBGpthread_cond_signal(&condJoin, ident);
	
	/*si crea il figlio*/
	index=++indiceGlob;
	rc = pthread_create(&pth, NULL, Notifica, (void*) index);
	if (rc) PrintERROR_andExit(errno, "errore in init");
	printf("creato thread %" PRIiPTR "\n", index); fflush(stdout);
	
	/* alloco la struttura in cui restituire il risultato */
	ptr=malloc(sizeof(int));
	if(ptr==NULL) PrintERROR_andExit(errno, "error in malloc");
	else *ptr=index;
	/*rilascio la mutua esclusione e termino*/
	DBGpthread_mutex_unlock(&mutex, ident);
	pthread_exit (ptr);  /* valore restituito dal thread */
}

int main() {
	int rc, i;
	pthread_t pth;
	intptr_t index;
	
	join=0;
	indiceGlob=INIZ-1;
	
	rc = pthread_mutex_init(&mutex, NULL);
	if (rc) PrintERROR_andExit(errno, "errore in init");
	rc = pthread_cond_init(&condAspettaJoin, NULL);
	if (rc) PrintERROR_andExit(errno, "errore in init");
	rc = pthread_cond_init(&condJoin, NULL);
	if (rc) PrintERROR_andExit(errno, "errore in init");
	
	for(i=0; i<INIZ; i++) {
		index=i;
		rc = pthread_create(&pth, NULL, Notifica, (void*) index);
		if (rc) PrintERROR_andExit(errno, "errore in init");
	}
	
	while(1) {
		void* ricevuto;
		DBGpthread_mutex_lock(&mutex, "main");
		while(!join) {
			/*aspettiamo di poter joinare qualcuno*/
			DBGpthread_cond_wait(&condJoin, &mutex, "main");
		}
		/*joiniamo*/
		rc = pthread_join(daJoinare, &ricevuto);
		if(rc) PrintERROR_andExit(errno, "error in join");
		
		printf("joinato thread %d\n", *((int*)ricevuto)); fflush(stdout);
		free(ricevuto);
		join=0;
		DBGpthread_cond_signal(&condAspettaJoin, "main");
		DBGpthread_mutex_unlock(&mutex, "main");
	}	
}
