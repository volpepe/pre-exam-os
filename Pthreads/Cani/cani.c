/*cani.c*/

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <inttypes.h>
#include <pthread.h> 

#include "printerror.h"
#include "DBGpthread.h"

#define NUM_CANI 10
#define DX 0
#define SX 1
#define POSTI 2

int distrib, turno, caniCavigliaSX, caniCavigliaDX;
pthread_mutex_t mutexTurno, mutexAzzanna;
pthread_cond_t condFila, condPrimo;

void* Cane(void* arg) {
	char ident[32];
	intptr_t i = (intptr_t)arg;
	int cavigliaScelta, mioTurno;
	
	sprintf(ident, "cane [%" PRIiPTR "]", i);
	
	while(1) {
		DBGpthread_mutex_lock(&mutexTurno, ident);
		/*ognuno prende il suo pìosto in fila*/
		mioTurno=distrib++;
		DBGpthread_mutex_unlock(&mutexTurno, ident);
		
		DBGpthread_mutex_lock(&mutexAzzanna, ident);
		/*se non è il nostro turno aspettiamo in fila*/
		while (mioTurno!=turno) {
			DBGpthread_cond_wait(&condFila, &mutexAzzanna, ident);
			if (mioTurno!=turno) DBGpthread_cond_signal(&condFila, ident);
		}
		/*sono il primo della fila: controllo se ci sono caviglie libere**/
		while (caniCavigliaSX>=POSTI && caniCavigliaDX>=POSTI) {
			DBGpthread_cond_wait(&condPrimo, &mutexAzzanna, ident);
		}
		/*qua dico ai cani in fila che possono cercare di capire chi è il primo della fila*/
		DBGpthread_cond_signal(&condFila, ident);
		/*nel frattempo vado avanti*/
		if (caniCavigliaSX<POSTI) {
			cavigliaScelta=SX; caniCavigliaSX++; printf("%s azzanna caviglia sinistra\n", ident); fflush(stdout);
		} else {
			cavigliaScelta=DX; caniCavigliaDX++; printf("%s azzanna caviglia destra\n", ident); fflush(stdout);
		}
		
		DBGpthread_mutex_unlock(&mutexAzzanna, ident);
		/*azzanno e vengo calciato via*/
		DBGnanosleep(500000000, ident);
		DBGpthread_mutex_lock(&mutexAzzanna, ident);
		
		printf("%s viene scalciato via da caviglia %s\n", ident, cavigliaScelta==DX? "dx" : "sx"); fflush(stdout);
		cavigliaScelta==DX? caniCavigliaDX-- : caniCavigliaSX--;
		/*segnalo il primo della fila perchè si è liberato un posto*/
		DBGpthread_cond_signal(&condPrimo, ident);
		DBGpthread_mutex_unlock(&mutexAzzanna, ident);
	}
}

int main() {
	pthread_t th; 
	int  rc, i;
	intptr_t index;
	
	distrib=0; turno=0; caniCavigliaDX=0; caniCavigliaSX=0;

	rc = pthread_cond_init(&condFila, NULL);
	if( rc ) PrintERROR_andExit(errno,"pthread_cond_init failed");
	rc = pthread_cond_init(&condPrimo, NULL);
	if( rc ) PrintERROR_andExit(errno,"pthread_cond_init failed");

	rc = pthread_mutex_init(&mutexAzzanna, NULL); 
	if( rc ) PrintERROR_andExit(errno,"pthread_mutex_init failed");
	rc = pthread_mutex_init(&mutexTurno, NULL); 
	if( rc ) PrintERROR_andExit(errno,"pthread_mutex_init failed");

	for(i=0;i<NUM_CANI;i++) {
		index=i;
		rc=pthread_create(&th,NULL,Cane,(void*)index); 
		if(rc) PrintERROR_andExit(errno,"pthread_create failed");
	}
	
	pthread_exit(NULL);
	return(0); 
}
