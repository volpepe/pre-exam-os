/*ex 35: masochisti*/

#ifndef _THREAD_SAFE
	#define _THREAD_SAFE
#endif
#ifndef _POSIX_C_SOURCE
	#define _POSIX_C_SOURCE 200112L
#endif

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <inttypes.h>
#include <pthread.h> 

#include "printerror.h"
#include "DBGpthread.h"

#define A 0
#define B 1
#define N_TYPE_A 3
#define N_TYPE_B 4

pthread_mutex_t mutex;
pthread_cond_t condQueue[2], condGo;

/*global variables*/
int pres[2];

void *Car(void* arg) {
	intptr_t index = (intptr_t)arg;
	char ident[32];
	int type = index<N_TYPE_A? A : B;

	sprintf(ident, "[%" PRIiPTR "]: type %c", index, type==A? 'A' : 'B');

	while (1) {
		DBGpthread_mutex_lock(&mutex, ident);
		/*a car has to wait if it is not the first of the line of its type*/
		while (pres[type]) {
			DBGpthread_cond_wait(&condQueue[type], &mutex, ident);
		}

		/*from this point on, a car is the first in line*/
		pres[type] = 1;
		/*all the other cars will wait*/
		/*we need to wait for the car on the other side to be ready*/
		if ( !pres[((type+1)%2)] ) {
			/*fancy way to say: if the other pres variable is 0*/
			printf("masochist %s waits on one side of the bridge\n", ident); fflush(stdout);
			DBGpthread_cond_wait(&condGo, &mutex, ident);
			/*since this will be the last car to cross the bridge, it has 
			to wake up two other cars*/
			DBGpthread_cond_signal(&condQueue[type], ident);
			DBGpthread_cond_signal(&condQueue[((type+1)%2)], ident);
			pres[type] = 0;
			pres[((type+1)%2)] = 0;
		} else {
			/*the car which arrives here is the second one: the two cars are ready
			to go*/
			DBGpthread_cond_signal(&condGo, ident);
			/*the second car still has the mutex: it is the first one to go*/
		}
		printf("masochist %s says: AAAAAAAAH\n", ident); fflush(stdout);
		DBGpthread_mutex_unlock(&mutex, ident);

		/*the car goes round and round and round*/
		DBGsleep(2, ident);
	}
}

int main(void) {
	pthread_t pth;
	int contr, i;
	intptr_t index;
	char ident[32];

	sprintf(ident, "main");

	pres[0] = 0; pres[1] = 0;

	/*init*/
	contr = pthread_mutex_init(&mutex, NULL);
	if (contr) PrintERROR_andExit(errno, "error in main init");
	contr = pthread_cond_init(&condQueue[0], NULL);
	if (contr) PrintERROR_andExit(errno, "error in main init");
	contr = pthread_cond_init(&condQueue[1], NULL);
	if (contr) PrintERROR_andExit(errno, "error in main init");
	contr = pthread_cond_init(&condGo, NULL);
	if (contr) PrintERROR_andExit(errno, "error in main init");

	/*pthreads creation*/
	for (i=0; i<( N_TYPE_A + N_TYPE_B ); i++) {
		index = i;
		contr = pthread_create( &pth, NULL, Car, (void*) index );
		if ( contr ) PrintERROR_andExit(contr, "error in main pthread_create");
	}

	/*main ends*/
	pthread_exit(NULL);
	return 0;
}