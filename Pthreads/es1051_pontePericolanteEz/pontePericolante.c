/* file:  pontePericolante.c */

#ifndef _THREAD_SAFE
	#define _THREAD_SAFE
#endif
#ifndef _POSIX_C_SOURCE
	#define _POSIX_C_SOURCE 200112L
#endif

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <inttypes.h>
#include <pthread.h> 

#include "printerror.h"
#include "DBGpthread.h"

#define ORARIO 0
#define ANTI 1

#define NUM_OR 4
#define NUM_AN 4

/*var da proteggere*/
int distrib[2], turno[2], inAttesa[2], ponteLibero;

pthread_mutex_t mutexPonte, mutexTurno;
pthread_cond_t waitTurno[2]; /*una per lato*/

void *Macchina (void* arg) {
	intptr_t index = (intptr_t) arg;
	int i = index;
	int biglietto, senso = i<NUM_OR ? ORARIO : ANTI;
	char ident[32];

	sprintf(ident, "Macchina in senso %s", senso==ORARIO? "orario" : "antiorario");

	while(1) {
		DBGpthread_mutex_lock(&mutexTurno, ident);
		/*prendiamo il biglietto*/
		biglietto=distrib[senso];
		distrib[senso]++;
		DBGpthread_mutex_unlock(&mutexTurno, ident);

		DBGpthread_mutex_lock(&mutexPonte, ident);
		/*per andare dobbiamo controllare in ordine che:
		-il ponte sia vuoto
		-sia il nostro turno
		più in basso segnaleremo il lato con più macchine quindi non ci sarà da preoccuparsi
		anche di questo*/
		while ( !ponteLibero || turno[senso]!=biglietto ) {
			inAttesa[senso]++;
			DBGpthread_cond_wait(&waitTurno[senso], &mutexPonte, ident);
			/*se non vado bene ne sveglio un altro*/
			if ( !ponteLibero || turno[senso]!=biglietto ) DBGpthread_cond_signal(&waitTurno[senso], ident);
			inAttesa[senso]--;
		}

		/*quando usciamo di qui siamo SICURI che il ponte è libero e che è il nostro turno*/
		/*occupiamo il ponte e aggiorniamo il turno*/
		ponteLibero=0; turno[senso]++;

		/*un secondo per percorrere il ponte*/
		DBGpthread_mutex_unlock(&mutexPonte, ident);
		printf("%s con biglietto %d percorre il ponte\n", ident, biglietto); fflush(stdout);
		DBGsleep(1, ident);

		DBGpthread_mutex_lock(&mutexPonte, ident);
		/*dobbiamo segnalare una delle auto sul lato con più auto in attesa*/
		if (inAttesa[senso]==inAttesa[(senso+1)%2]) DBGpthread_cond_signal(&waitTurno[ORARIO], ident);
		else inAttesa[senso]>inAttesa[(senso+1)%2] ? DBGpthread_cond_signal(&waitTurno[senso], ident) :
		DBGpthread_cond_signal(&waitTurno[(senso+1)%2], ident);
		/*sgomberiamo il ponte*/
		ponteLibero=1;
		printf("Il ponte è di nuovo libero\n"); fflush(stdout);
		DBGpthread_mutex_unlock(&mutexPonte, ident);

		DBGsleep(5, ident);
	}
	pthread_exit(NULL);
}

int main(void)
{
	int rc, i;
	intptr_t index;
	pthread_t pth;
	char ident[32];

	sprintf(ident, "main");

	rc = pthread_mutex_init(&mutexTurno, NULL);
	if (rc) PrintERROR_andExit(errno, "init failed");
	rc = pthread_mutex_init(&mutexPonte, NULL);
	if (rc) PrintERROR_andExit(errno, "init failed");
	rc = pthread_cond_init(&waitTurno[0], NULL);
	if (rc) PrintERROR_andExit(errno, "init failed");
	rc = pthread_cond_init(&waitTurno[1], NULL);
	if (rc) PrintERROR_andExit(errno, "init failed");

	ponteLibero=1;
	for (i=0; i<2; i++) { turno[i]=0; distrib[i]=0; inAttesa[i]=0; }

	for (i=0; i<(NUM_AN+NUM_OR); i++) {
		index=i;
		rc = pthread_create(&pth, NULL, Macchina, (void*)index );
		if (rc) PrintERROR_andExit(errno, "create failed");
	}

	pthread_exit(NULL);
	return 0;
}