/* file:  megapizze_complicato.c */

/*THREAD_SAFE e POSIX_C_SOURCE già definiti nel makefile*/

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <inttypes.h>
#include <pthread.h> 

#include "printerror.h"
#include "DBGpthread.h"

#define N_CLIENTI 5
#define POSTI 4

int personeSedute, pizzaPresente, personeInAttesaFuori, pizzaioloSegnalato;

pthread_cond_t condTavolo, condFuori, condPizzaiolo;
pthread_mutex_t mutex;

void *Cliente(void* arg) {
	intptr_t index = (intptr_t)arg;
	int i = index;
	char ident[32];
	
	sprintf(ident, "Cliente %d", i);
	
	while (1) {	
		DBGpthread_mutex_lock(&mutex, ident);
		/*dobbiamo controllare se c'è posto e, se il posto c'è, se è perchè le persone se ne stanno andando
		 * (e in tal caso dobbiamo aspettare che se ne siano andate tutte) 
		 * o se quelle persone stanno aspettando compagnia*/
		while (personeSedute>=POSTI) {
			personeInAttesaFuori++;
			printf("%s attende fuori dal locale\n", ident); fflush(stdout);
			DBGpthread_cond_wait(&condFuori, &mutex, ident);
			personeInAttesaFuori--;
		}
		/*qui entriamo nel locale perchè c'è almeno un posto libero e le persone non hanno
		 * ancora mangiato.*/
		 
		personeSedute++;
		printf("%s si siede al tavolo\n", ident); fflush(stdout);
		/*se siamo i terzi, svegliamo il pizzaiolo*/
		if (personeSedute==3) {
			pizzaioloSegnalato=1;
			DBGpthread_cond_signal(&condPizzaiolo, ident);
		}
		/*se siamo i quarti e la pizza è già arrivata svegliamo tutti gli altri*/
		if (personeSedute==POSTI && pizzaPresente) DBGpthread_cond_broadcast(&condTavolo, ident);
		else /*aspettiamo di essere svegliati o dalla quarta persona o dal pizzaiolo*/
			DBGpthread_cond_wait(&condTavolo, &mutex, ident);
		/*arrivati qui siamo stati svegliati: possiamo mangiare*/
		DBGpthread_mutex_unlock(&mutex, ident);
		
		DBGsleep(3, ident);
		
		DBGpthread_mutex_lock(&mutex, ident);
		printf("%s ha mangiato e se ne va\n", ident); fflush(stdout);
		personeSedute--;
		/*se sono l'ultimo che si alza dal tavolo chiamo la gente fuori dal locale se ce n'è per dirgli che il
		 * tavolo è libero*/
		if (personeSedute==0) {
			pizzaPresente=0;
			if (personeInAttesaFuori>0) DBGpthread_cond_broadcast(&condFuori, ident);
		}
		DBGpthread_mutex_unlock(&mutex, ident);
		DBGsleep(2+i, ident);	
	}
	pthread_exit(NULL);
}

void *Pizzaiolo(void* arg) {
	
	DBGpthread_mutex_lock(&mutex, "Pizzaiolo");
	/*il pizzaiolo aspetta di essere chiamato per portare le pizze*/
	while (1) {
		if (!pizzaioloSegnalato) DBGpthread_cond_wait(&condPizzaiolo, &mutex, "Pizzaiolo");
		/*quando viene chiamato imposta*/
		pizzaioloSegnalato=0;
		printf("Pizzaiolo porta la pizza ai %d clienti attualmente seduti\n", personeSedute);
		pizzaPresente=1;
		/*poi, se ci sono 4 clienti, dice loro che è arrivata la pizza*/
		/*se ce ne sono 3, sarà il quarto che arriva a verificare se c'è una pizza e
		 * dire agli altri di svegliarsi*/
		if (personeSedute==POSTI) DBGpthread_cond_broadcast(&condTavolo, "Pizzaiolo");
		/*infine ricomincia ad aspettare*/
	}
	/*mai raggiunto*/
	DBGpthread_mutex_unlock(&mutex, "Pizzaiolo");
	pthread_exit(NULL);
}

int main() { 
	int rc, i;
	pthread_t pth;
	intptr_t index;
	
	personeSedute=0;
	pizzaPresente=0;
	personeInAttesaFuori=0;
	pizzaioloSegnalato=0;

	rc = pthread_cond_init(&condTavolo, NULL);
	if( rc ) PrintERROR_andExit(errno,"pthread_cond_init failed");
	rc = pthread_cond_init(&condFuori, NULL);
	if( rc ) PrintERROR_andExit(errno,"pthread_cond_init failed");
	rc = pthread_cond_init(&condPizzaiolo, NULL);
	if( rc ) PrintERROR_andExit(errno,"pthread_cond_init failed");
	rc = pthread_mutex_init(&mutex, NULL); 
	if( rc ) PrintERROR_andExit(errno,"pthread_mutex_init failed");


	for(i=0;i<N_CLIENTI;i++) {
		index=i;
		rc=pthread_create(&pth,NULL, Cliente,(void*)index); 
		if(rc) PrintERROR_andExit(errno,"pthread_create failed");
	}

	rc = pthread_create(&pth, NULL, Pizzaiolo,(void*)NULL); 
	if(rc) PrintERROR_andExit(errno,"pthread_create failed");
	
	pthread_exit(NULL);
	return(0); 
} 
