/* file:  megapizze_semplice.c */

/*I define importanti sono già all'interno del Makefile*/

#include "printerror.h"
#include "DBGpthread.h"

#include <unistd.h>   /* exit() etc */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>     /* per strerror_r  and  memset */
#include <sys/mman.h>  /* shm_* stuff, and mmap() */
#include <sys/types.h>
#include <signal.h>
#include <sys/time.h>	/* timeval{} for select() */
#include <time.h>	/* timespec{} for pselect() */
#include <limits.h>	/* for OPEN_MAX */
#include <errno.h>
#include <sys/wait.h>
#include <assert.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdint.h>
#include <inttypes.h>

#include <fcntl.h>
#include <sys/stat.h>

#define CLIENTI 5
#define POSTI 4

typedef struct {
	int personeAlTavolo, personeInCoda, personeHannoMangiato;
	pthread_cond_t condCoda, condTavolo;
	pthread_mutex_t mutex;
} Pizzeria;

Pizzeria* p;

void *Cliente(int i) {
	char ident[32];
	
	sprintf(ident, "Cliente %d", i);
	
	while(1) {
		DBGpthread_mutex_lock(&p->mutex, ident);
		/*entriamo in pizzeria e controlliamo se il tavolo ha posti liberi.
		 * Dobbiamo anche controllare che se ci sono posti liberi non è perchè
		 * la gente se ne sta andando: infatti in tal caso dobbiamo aspettare che
		 * se ne siano andati tutti prima di sederci*/
		while (p->personeAlTavolo>=POSTI || p->personeHannoMangiato==1) {
			printf("%s attende in coda\n", ident); fflush(stdout);
			p->personeInCoda++;
			DBGpthread_cond_wait(&p->condCoda, &p->mutex, ident);
			p->personeInCoda--;
		}
		
		/*arrivati qua ci possiamo sedere al tavolo pechè sicuramente c'è almeno
		 * un posto libero e le persone non hanno ancora mangiato*/
		 p->personeAlTavolo++;
		 printf("%s si è seduto\n", ident); fflush(stdout);
		 /*se siamo la quarta persona che arriva dobbiamo iniziare a mangiare*/
		 if (p->personeAlTavolo==POSTI) {
			 printf("Una pizza è comparsa sul tavolo!\n");
			 DBGpthread_cond_broadcast(&p->condTavolo, ident);
		 } else {
			 /*il quarto non si mette ad aspettare ovviamente*/
			 DBGpthread_cond_wait(&p->condTavolo, &p->mutex, ident);
		 }
		 /*a sto punto mangiamo*/
		 DBGpthread_mutex_unlock(&p->mutex, ident);
		 
		 DBGsleep(6, ident);
		 
		 DBGpthread_mutex_lock(&p->mutex, ident);
		 /*il primo che finisce di mangiare imposta gli stati utili*/
		 if (p->personeAlTavolo==POSTI) {
			 printf("I clienti hanno finito di mangiare\n");
			 p->personeHannoMangiato=1;
		 }
		 p->personeAlTavolo--;
		 /*l'ultimo che va via dal tavolo chiama quelli in coda e imposta 
		  * personeHannoMangiato a 0*/
		 if (p->personeAlTavolo==0) {
			 p->personeHannoMangiato=0;
			 DBGpthread_cond_broadcast(&p->condCoda, ident);
		 }
		 printf("%s esce dal locale\n", ident);
		 
		 DBGpthread_mutex_unlock(&p->mutex, ident);
		 
		 DBGsleep(2+i, ident);
	}
}

int main () 
{ 
	int shmfd, rc, i;
	pid_t pid;
	int shared_seg_size = sizeof(Pizzeria);
	pthread_mutexattr_t mattr;
	pthread_condattr_t cvattr;

	shmfd = shm_open( "/megapizze", O_CREAT | O_RDWR, S_IRWXU );
	if (shmfd < 0) PrintERROR_andExit(errno, "Error in shm_open");
	
	rc = ftruncate(shmfd, shared_seg_size);
	if (rc != 0) PrintERROR_andExit(errno, "ftruncate() failed");

	p = (Pizzeria*)mmap(NULL, shared_seg_size,
		PROT_READ|PROT_WRITE, MAP_SHARED, shmfd, 0);
	if( p==MAP_FAILED )
	 		PrintERROR_andExit(errno,"mmap failed");
	
	p->personeAlTavolo=0;
	p->personeInCoda=0;
	p->personeHannoMangiato=0;

	rc=pthread_mutexattr_init(&mattr);
	if( rc ) PrintERROR_andExit(errno,"pthread_mutexattr_init failed");
	rc=pthread_mutexattr_setpshared(&mattr,PTHREAD_PROCESS_SHARED);
	if( rc ) PrintERROR_andExit(errno,"pthread_mutexattr_setpshared failed");
	rc=pthread_condattr_init(&cvattr);
	if( rc ) PrintERROR_andExit(errno,"pthread_condattr_init failed");
	rc=pthread_condattr_setpshared(&cvattr,PTHREAD_PROCESS_SHARED);
	if( rc ) PrintERROR_andExit(errno,"pthread_condattr_setpshared failed");


	rc = pthread_cond_init(&p->condCoda, &cvattr);
	if( rc ) PrintERROR_andExit(errno,"pthread_cond_init failed");
	rc = pthread_cond_init(&p->condTavolo, &cvattr);
	if( rc ) PrintERROR_andExit(errno,"pthread_cond_init failed");

	rc = pthread_mutex_init(&p->mutex, &mattr); 
	if( rc ) PrintERROR_andExit(errno,"pthread_mutex_init failed");

	for(i=0;i<CLIENTI;i++) {
		pid=fork();
		if(pid<0) 
			PrintERROR_andExit(errno,"fork failed");
		else if(pid==0) {
			/*figlio*/
			Cliente(i);
			exit(0);
		}
	}
		
	return(0); 
} 
  
  
