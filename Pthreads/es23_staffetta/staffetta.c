/*file: staffetta.c*/

/*THREAD_SAFE and C_POSIX_SOURCE are already defined by the makefile*/

#include "printerror.h"

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <inttypes.h>
#include <pthread.h> 
#include <math.h>

#include "DBGpthread.h"

#define NTHR 20
#define NEXIT 5

float fglob;
int turn;

pthread_mutex_t mutex;
pthread_cond_t condExit, condTurn;

void *Staffetta(void* arg) 
{
	intptr_t index = (intptr_t)arg;
	int i = index;
	char ident[32];

	sprintf(ident, "staffetta %d", i);

	DBGpthread_mutex_lock(&mutex, ident);
	while (i!=turn) {
		DBGpthread_cond_wait(&condTurn, &mutex, ident);
		if (i!=turn) DBGpthread_cond_signal(&condTurn, ident);
	}

	/*assert(i==turn)*/
	turn++;
	DBGpthread_mutex_unlock(&mutex, ident);
	DBGsleep(1, ident);
	DBGpthread_mutex_lock(&mutex, ident);
	fglob=sqrt(fglob);
	printf("fglob is %f\n", fglob); fflush(stdout);

	/*we need to wait for 5 threads before exiting*/
	DBGpthread_cond_signal(&condTurn, ident);
	if (turn%NEXIT!=0) {
		DBGpthread_cond_wait(&condExit, &mutex, ident);
	} else DBGpthread_cond_broadcast(&condExit, ident);

	DBGpthread_mutex_unlock(&mutex, ident);
	printf("process coming out!\n"); fflush(stdout);

	pthread_exit(NULL);
}

int main(void) 
{
	pthread_t pth[NTHR];
	int rc, i;
	intptr_t index;
	char ident[32];

	sprintf(ident, "main");

	rc = pthread_mutex_init(&mutex, NULL);
	if (rc) PrintERROR_andExit(errno, "init failed");
	rc = pthread_cond_init(&condExit, NULL);
	if (rc) PrintERROR_andExit(rc, ident);
	rc = pthread_cond_init(&condTurn, NULL);
	if (rc) PrintERROR_andExit(rc, ident);

	fglob=1111;

	while (1) {
		turn=0;
		for (i=0; i<NTHR; i++) {
			index=i;
			rc = pthread_create(&pth[index], NULL, Staffetta, (void*) index);
			if (rc) PrintERROR_andExit(rc, ident);
		}

		for (i=0; i<NTHR; i++) {
			rc = pthread_join(pth[i], NULL);
			if (rc) PrintERROR_andExit(rc, ident);
		}
	}
}