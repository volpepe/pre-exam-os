/*elefantischizzinosi.c*/

/*THREAD_SAFE and C_POSIX_SOURCE are already defined by the makefile*/

#include "printerror.h"

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <inttypes.h>
#include <pthread.h> 

#include "DBGpthread.h"

#define MAX_LIT 500
#define ELEFANTI 5
#define LIT_BEV_ELEF 100
#define TEMPO_BEV_ELEF 100000000	/*usare nanosleep*/
#define PEKARI 10
#define LIT_BEV_PEK 1
#define TEMPO_BEV_PEK 10000000		/*usare nanosleep*/
#define RIEMPI 70

int pekariInBevuta, elefantiInBevuta;
int acqua;

pthread_mutex_t mutexBarriera, mutexLago;
pthread_cond_t condBarriera, condLagoElef, condLagoPeka;

void *Elefante(void* arg) {
	intptr_t index = (intptr_t) arg;
	char ident[32];

	sprintf(ident, "Elefante % " PRIiPTR "", index);

	while(1) {
		DBGpthread_mutex_lock(&mutexBarriera, ident);
		while (pekariInBevuta>0 || acqua-((elefantiInBevuta+1)*LIT_BEV_ELEF+pekariInBevuta*LIT_BEV_PEK)<0) 
		{
			/*non posso bere quindi mi metto in condBarriera ad aspettare*/
			DBGpthread_cond_wait(&condBarriera, &mutexBarriera, ident);
		}
		/*se arrivo qua sono al lago e posso mettermi a bere*/

		DBGpthread_mutex_lock(&mutexLago, ident);
		/*devo aggiornare anche la barriera*/
		elefantiInBevuta++;
		if(elefantiInBevuta+pekariInBevuta>1) {
			DBGpthread_mutex_unlock(&mutexBarriera, ident);
			DBGpthread_cond_wait(&condLagoElef, &mutexLago, ident);
			DBGpthread_mutex_lock(&mutexBarriera, ident);
			if (elefantiInBevuta>1) DBGpthread_cond_signal(&condLagoElef, ident);
		}
		/*se arrivo qui posso bere*/
		acqua-=LIT_BEV_ELEF;
		DBGpthread_mutex_unlock(&mutexLago, ident);
		DBGpthread_mutex_unlock(&mutexBarriera, ident);
		printf("%s beve\n", ident); fflush(stdout);
		DBGnanosleep(TEMPO_BEV_ELEF, ident);
		DBGpthread_mutex_lock(&mutexBarriera, ident);
		DBGpthread_mutex_lock(&mutexLago, ident);
		/*ho bevuto: imposto tutte le cose*/
		elefantiInBevuta--;
		DBGpthread_cond_signal(&condLagoElef, ident);
		/*non ha senso risvegliare gente alla barriera perchè non ho aggiunto acqua nè
		cambiato lo stato di puòbere*/
		DBGpthread_mutex_unlock(&mutexLago, ident);
		DBGpthread_mutex_unlock(&mutexBarriera, ident);
		DBGsleep(1, ident);
	}
}

void *Pekari(void* arg) {
	intptr_t index = (intptr_t) arg;
	char ident[32];

	sprintf(ident, "Pekaro % " PRIiPTR "", index);

	while(1) {
		DBGpthread_mutex_lock(&mutexBarriera, ident);
		while (acqua-((elefantiInBevuta+1)*LIT_BEV_ELEF+pekariInBevuta*LIT_BEV_PEK)<0) 
		{
			/*non posso bere quindi mi metto in condBarriera ad aspettare*/
			DBGpthread_cond_wait(&condBarriera, &mutexBarriera, ident);
		}
		/*se arrivo qua sono al lago e posso mettermi a bere*/

		DBGpthread_mutex_lock(&mutexLago, ident);
		/*devo aggiornare anche la barriera*/
		pekariInBevuta++;
		if(pekariInBevuta+elefantiInBevuta>1) {
			DBGpthread_mutex_unlock(&mutexBarriera, ident);
			DBGpthread_cond_wait(&condLagoPeka, &mutexLago, ident);
			DBGpthread_mutex_lock(&mutexBarriera, ident);
			if (pekariInBevuta>1) DBGpthread_cond_signal(&condLagoPeka, ident);
		}
		/*se arrivo qui posso bere*/
		acqua-=LIT_BEV_PEK;
		DBGpthread_mutex_unlock(&mutexLago, ident);
		DBGpthread_mutex_unlock(&mutexBarriera, ident);
		printf("%s beve\n", ident); fflush(stdout);
		DBGnanosleep(TEMPO_BEV_PEK, ident);
		DBGpthread_mutex_lock(&mutexBarriera, ident);
		DBGpthread_mutex_lock(&mutexLago, ident);
		/*ho bevuto: imposto tutte le cose*/
		pekariInBevuta--;
		DBGpthread_cond_signal(&condLagoPeka, ident);
		/*ha senso risvegliare gente alla barriera perchè potrei essere l'ultimo pekaro*/
		DBGpthread_cond_broadcast(&condBarriera, ident);
		DBGpthread_mutex_unlock(&mutexLago, ident);
		DBGpthread_mutex_unlock(&mutexBarriera, ident);
		DBGsleep(2, ident);
		DBGnanosleep(200000000, ident);
	}
}

void *Acquedotto(void* arg) {
	char ident[32];

	sprintf(ident, "Acquedotto");
	while(1) {
		DBGpthread_mutex_lock(&mutexBarriera, ident);
		DBGpthread_mutex_lock(&mutexLago, ident);
		acqua+=RIEMPI; if(acqua>500) acqua=500;
		printf("%s aggiunge acqua nel lago. acqua=%d\n", ident, acqua); fflush(stdout);
		DBGpthread_cond_broadcast(&condBarriera, ident);
		DBGpthread_mutex_unlock(&mutexLago, ident);
		DBGpthread_mutex_unlock(&mutexBarriera, ident);
		/*sveglio tutti*/
		DBGsleep(1, ident);
	}
}

int main(void) {
	int rc, i;
	intptr_t index;
	char ident[32];
	pthread_t pth;

	sprintf(ident, "main");

	pekariInBevuta=0;
	elefantiInBevuta=0;
	acqua=MAX_LIT;

	rc=pthread_mutex_init(&mutexBarriera, NULL);
	if (rc) PrintERROR_andExit(errno, ident);
	rc=pthread_mutex_init(&mutexLago, NULL);
	if (rc) PrintERROR_andExit(errno, ident);
	rc=pthread_cond_init(&condBarriera, NULL);
	if (rc) PrintERROR_andExit(errno, ident);
	rc=pthread_cond_init(&condLagoPeka, NULL);
	if (rc) PrintERROR_andExit(errno, ident);
	rc=pthread_cond_init(&condLagoElef, NULL);
	if (rc) PrintERROR_andExit(errno, ident);

	for(i=0; i<ELEFANTI; i++) {
		index=i;
		rc = pthread_create(&pth, NULL, Elefante, (void*) index);
		if (rc) PrintERROR_andExit(rc, ident);
	}

	for(i=0; i<PEKARI; i++) {
		index=i;
		rc = pthread_create(&pth, NULL, Pekari, (void*) index);
		if (rc) PrintERROR_andExit(rc, ident);
	}

	rc = pthread_create(&pth, NULL, Acquedotto, (void*) NULL);
	if (rc) PrintERROR_andExit(rc, ident); 

	pthread_exit(NULL);
	return 0;
}
