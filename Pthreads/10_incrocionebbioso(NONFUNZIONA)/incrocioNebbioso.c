/* file:  incrocioNebbioso.c */

#ifndef _THREAD_SAFE
	#define _THREAD_SAFE
#endif
#ifndef _POSIX_C_SOURCE
	#define _POSIX_C_SOURCE 200112L
#endif

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <inttypes.h>
#include <pthread.h> 

#include "printerror.h"
#include "DBGpthread.h"

/*nord, ovest, sud ed est*/
#define N 0
#define E 1
#define S 2
#define W 3
#define LATON_E 0
#define LATOS_W 1
#define AUTON_E 5
#define AUTOS_W 10

/*variabili da proteggere*/
int macchineSuLato[4], attraversoIo, nebbia;
/*attraversoIo contiene il lato da cui è partita la macchina in attraversamento*/
/*quando nessuno sta attraversando assume un valore sentinella (-1)*/

pthread_mutex_t mutex;
pthread_cond_t condLato[4];

int tuttiBloccati() {
	/*questa funzione viene SEMPRE eseguita in mutua esclusione*/
	int i=0;
	for (; i<4; i++) {
		if (macchineSuLato[i]<=0) return 0;
	} return 1;
}

int hoPrecedenza(int lato) {
	/*questa funzione viene SEMPRE eseguita in mutua esclusione*/
	if (macchineSuLato[(lato-1)%4]>0) {
		return 0;
	}
	return 1;
}

int possoPassare(int lato) {
	/*questa funzione viene SEMPRE eseguita in mutua esclusione*/
	if (attraversoIo!=-1 && (attraversoIo==(lato+1)%4 || attraversoIo==(lato-1)%4))
		return 0;
	return 1;
}

void *Auto(void* arg) {
	intptr_t index = (intptr_t) arg;
	int verso = index<AUTON_E? LATON_E : LATOS_W;
	int lato;
	char ident[32];

	if (verso==LATON_E) lato=N;
	else {
		if (index<AUTOS_W) lato=S;
		else lato=W;
	}
	sprintf(ident, "Auto %" PRIiPTR "", index);

	while(1) {
		DBGpthread_mutex_lock(&mutex, ident);
		macchineSuLato[lato]++;
		/*che controlli devo fare per poter partire?
		1) non deve esserci la nebbia
		2) se c'è almeno una macchina che può attraversare, per essere io
		non devo avere nessuno a destra (per la precedenza)
		3) se non ho nessuno a destra, attraversoIo deve assumere il valore
		mio opposto o -1
		a questo punto sono libero di attraversare*/
		while (nebbia || !hoPrecedenza(lato) || !possoPassare(lato)) {
			/*attenzione: dobbiamo controllare di non essere in deadlock, 
			cioè in tutti i lati c'è almeno una macchina ferma*/
			/*se siamo in deadlock parto io*/
			if (tuttiBloccati()) break;
			/*potrei non avere la precedenza: in questo caso segnalo alla mia destra
			in quanto sono certo ci sia qualcuno*/
			if (!hoPrecedenza(lato)) DBGpthread_cond_signal(&condLato[(lato-1)%4], ident);
			DBGpthread_cond_wait(&condLato[lato], &mutex, ident);
		}
		/*qua posso attraversare perchè attraversoIo è uguale a -1, al mio lato o
		al limite al lato opposto: inoltre, non c'è la nebbia.*/
		macchineSuLato[lato]--;
		attraversoIo=lato;
		/*segnalo il lato opposto, il mio stesso lato e quelli alla mia sinistra*/
		DBGpthread_cond_signal(&condLato[(lato+2)%4], ident);
		DBGpthread_cond_signal(&condLato[(lato+1)%4], ident);
		DBGpthread_cond_signal(&condLato[lato], ident);
		DBGpthread_mutex_unlock(&mutex, ident);
		DBGnanosleep(100000000, ident);
		DBGpthread_mutex_lock(&mutex, ident);
		/*se nel frattempo il lato opposto o il mio lato ha finito gente reimposto a -1*/
		if (attraversoIo!=(lato+2)%4 || (attraversoIo==lato && macchineSuLato[lato]==0)) attraversoIo=-1;
		DBGpthread_mutex_unlock(&mutex, ident);
		printf("%s urla yeeeh attraversando l'incrocio da %d\n",ident, lato); fflush(stdout);
		DBGsleep(3, ident);
		/*la macchina arriva al suo altro lato possibile*/
		lato=(lato+1)%4;
	}
	pthread_exit(NULL);
}

void *Nebbia (void* arg) {
	int i;
	/*la nebbia si sveglia ogni 3 secondi e da fastidio per un secondo*/
	while (1) {
		DBGsleep(3, "nebbia");
		DBGpthread_mutex_lock(&mutex, "nebbia");
		nebbia=1;
		DBGpthread_mutex_unlock(&mutex, "nebbia");
		printf("cala la nebbia\n"); fflush(stdout);
		DBGsleep(1, "nebbia");
		DBGpthread_mutex_lock(&mutex, "nebbia");
		printf("la nebbia si dirada\n"); fflush(stdout);
		nebbia=0;
		/*sveglio uno su un lato a caso, il primo con un po' di gente*/
		for(i=0; i<4; i++) {
			if (macchineSuLato[i]>0) {
				DBGpthread_cond_signal(&condLato[i], "nebbia");
				break;
			}
		} 
		DBGpthread_mutex_unlock(&mutex, "nebbia");
	}
	pthread_exit(NULL);
}

int main() {
	int rc, i;
	intptr_t index;
	pthread_t pth;

	rc = pthread_mutex_init(&mutex, NULL);
	if(rc) PrintERROR_andExit(rc, "Errore in init");
	for (i=0; i<4; i++) {
		rc = pthread_cond_init(&condLato[i], NULL);
		if(rc) PrintERROR_andExit(rc, "Errore in init");
		macchineSuLato[i]=0;
	}
	attraversoIo=-1;
	nebbia=0;

	for(i=0; i<AUTOS_W+AUTON_E; i++) {
		index=i;
		rc = pthread_create(&pth, NULL, Auto, (void*) index);
		if(rc) PrintERROR_andExit(rc, "Errore in create");
	}

	rc = pthread_create(&pth, NULL, Nebbia, (void*) index);
	if(rc) PrintERROR_andExit(rc, "Errore in create");

	pthread_exit(NULL);
	return 0;
}
