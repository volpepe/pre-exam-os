/*file strada.c*/

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <inttypes.h>
#include <pthread.h> 

#include "printerror.h"
#include "DBGpthread.h"

#define ORIZZONTALE 0
#define VERTICALE 1
#define N_VERT 5
#define N_ORIZZ 10

int latoStrada[4];
int versoAttuale; /*1 o 0*/

pthread_mutex_t mutex, mutexControllo;
pthread_cond_t condPrec[2];

void *Auto (void* arg) {
	int
}

int main (int argc, char* argv[]) 
{ 
	pthread_t    th; 
	int  rc;
	intptr_t i;
	
	for(i=0; i<4; i++) latoStrada[i]=0;
	versoAttuale=ORIZZONTALE;

	rc = pthread_cond_init(&condPrec[ORIZZONTALE], NULL);
	if( rc ) PrintERROR_andExit(errno,"pthread_cond_init failed");
	rc = pthread_cond_init(&condPrec[VERTICALE], NULL);
	if( rc ) PrintERROR_andExit(errno,"pthread_cond_init failed");

	rc = pthread_mutex_init(&mutex, NULL); 
	if( rc ) PrintERROR_andExit(errno,"pthread_mutex_init failed");
	rc = pthread_mutex_init(&mutexControllo, NULL); 
	if( rc ) PrintERROR_andExit(errno,"pthread_mutex_init failed");

	for(i=0;i<N_ORIZZ+N_VERT;i++) {
		rc=pthread_create(&th,NULL,Macchina,(void*)i); 
		if(rc) PrintERROR_andExit(errno,"pthread_create failed");
	}
	rc=pthread_create(&th,NULL,Nebbia,(void*)i); 
	if(rc) PrintERROR_andExit(errno,"pthread_create failed");
	
	pthread_exit(NULL);
	return(0); 
}
