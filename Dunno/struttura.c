#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <inttypes.h>

typedef struct struttura {
	uint32_t i;
	uint8_t c;
	uint32_t i2;
} /*__attribute__ (( packed ))*/ STRUTTURA;

int main() 
{
	printf("size of STRUTTURA is: %d bytes\n", sizeof(STRUTTURA));
	return 0;
}