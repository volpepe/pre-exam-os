#include <unistd.h>
#include <stdlib.h>		/* serve per la funzione rand */
#include <stdio.h>

#define ProdottoAumentato( X, Y )	\
		do {								\
			X=X+1;							\
			printf("%g\n", X*Y);		\
		} while(0);
		
int main() {
	ProdottoAumentato(3, 5);
	ProdottoAumentato(2.1, 5);
	return 0;
}
