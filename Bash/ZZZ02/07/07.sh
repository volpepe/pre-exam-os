#!/bin/bash

(( COUNTER=0 ))
while (( ${COUNTER}<10 )); do
	sleep 10 &
	(( COUNTER=${COUNTER}+1 ))
done

for proc in `ps aux | grep "sleep" | cut -b 10-14`; do
	kill -9 ${proc}
done