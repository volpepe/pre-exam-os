#!/bin/bash

NEWEST=
for file in `find /usr/include/*.h`; do
	if [[ ${file} -nt ${NEWEST} ]]; then
		NEWEST=${file}
	fi
done

echo ${NEWEST}
cat ${NEWEST} | wc -l