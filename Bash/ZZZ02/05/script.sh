#!/bin/bash

read NEWEST
while (read NEWFILE); do
	if [[ ${NEWFILE} -nt ${NEWEST} ]]; then
		NEWEST=${NEWFILE}
	fi
done

echo $NEWEST
cat ${NEWEST} | wc -l