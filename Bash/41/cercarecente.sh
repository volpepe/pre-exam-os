#!/bin/bash

NEWEST=
for file in `find /usr/include/linux -mindepth 2 -name *.h`; do
	if [[ ${file} -nt ${NEWEST} ]]; then
		NEWEST=${file}
	fi
done

echo ${NEWEST}