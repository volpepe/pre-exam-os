#!/bin/bash

(( LINE=0 ))
for line in `cat cadutevic.txt`; do
	for field in `echo ${line}`; do
		if (( ${LINE}==2 )); then
			echo ${field} >> temp.txt
		fi
		(( LINE=(${LINE}+1)%4 ))
	done
done

sort temp.txt | uniq -c
rm -f temp.txt
