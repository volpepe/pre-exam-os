#!/bin/bash

mkdir es
cd es/
(( I=0 ))
while (( I<10 )); do
	mkdir "1.${I}"
	(( I=${I}+1 ))
done

sleep 5

for folder in `ls`; do
	mv ${folder} "2.${folder##*.}"
done 
