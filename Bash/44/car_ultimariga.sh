#/bin/bash

for file in `find /usr/include/linux/ -maxdepth 0 -name *f*.h`; do
	(( LINEE=`cat ${file} | wc -l` ))
	if (( ${LINEE}>10 && ${LINEE}<100 )); then
		echo "${file} ha `cat ${file} | tail -n 1 | wc -m` caratteri nell'ultima riga"
	fi
done
