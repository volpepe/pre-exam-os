#!/bin/bash

if (( $#==0 )); then
	echo "troppi pochi argomenti"
	exit 1
fi
if (( $#>9 )); then
	echo "troppi argomenti"
	exit 1
fi

NUM=1
COUNTODD=0
COUNTEVEN=0
while (( ${NUM}<=$# )); do
	if (( ${NUM}%2==0 )); then 
		(( COUNTEVEN=${COUNTEVEN} + `cat ${!NUM} | wc -l` ))
	else (( COUNTODD=${COUNTODD} + `cat ${!NUM} | wc -l` ))
	fi
	(( NUM=${NUM}+1 ))
done

echo ${COUNTEVEN}
echo ${COUNTODD} 1>&2

