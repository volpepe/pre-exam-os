#!/bin/bash

if (( $#!=1 )); then
	echo "numero argomenti errato"
	exit 1
fi

if [[ -e $1 ]]; then
	sleep 2 & cat $1 | tail -n 3 >> OUTPUT.txt &
	exit 0
else 
	echo "argomento non file" 1>&2
	exit 2
fi