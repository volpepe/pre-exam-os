#!/bin/bash

for file in `find /usr/include/linux/*.h`; do
	cat ${file} | grep int > temp.txt
	if [[ -s temp.txt ]]; then
		for line in `cat temp.txt`; do
			echo ${line:0:3}
		done
	fi
done

rm -f temp.txt