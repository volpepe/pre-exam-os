#!/bin/bash

for FILE in `find /usr/include/linux/ -name *.h`; do
	for LINE in `cat ${FILE} | grep "int"`; do
		echo ${LINE:0:3} >> output.txt
	done
done
