#!/bin/bash

if (( $#!=2 )); then
	echo "numero argomenti errato"
fi

EMAIL=$1
MATRICOLA=$2
exec {FD}<./matricola.txt
while read -u ${FD} nome cognome matricola materia; do
	if (( ${matricola}==${MATRICOLA} )); then 
		echo "la account ${EMAIL} appartiene allo studente ${nome} ${cognome}"
		exit 0
	fi
done

echo "la matricola non è stata trovata" >&2
exit 1