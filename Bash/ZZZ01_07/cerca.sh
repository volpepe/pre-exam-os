#!/bin/bash

for file in `find /usr/include/linux/netfilter/*.h`; do 
	INTLINES=`cat ${file} | grep "int"`
	if [[ ${#INTLINES}==0 ]]; then
		echo ${file}
	fi
donec