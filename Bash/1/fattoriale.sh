#!/bin/bash

NUM=$1
(( FATT=1 ))
while (( ${NUM}>1 )); do
	(( FATT=${NUM}*${FATT} ))
	(( NUM=${NUM}-1 ))
done
echo ${FATT}