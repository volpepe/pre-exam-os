#!/bin/bash

NUM=$1
while (( ${NUM}>1 )); do
	(( RIS=${RIS}*${NUM} ))
	(( NUM=${NUM}-1 ))
	source ./fattoriale1.sh "${NUM}"
done