#!/bin/bash

GOT=`ls /usr/include/*.h`
for file in ${GOT}; do
	(( NIFD=`cat ${file} | grep ifdef | wc -l` ))
	if (( ${NIFD} >= 10 )); then
		cat ${file} | grep ifdef | head -n 5  >> ifdefs.txt
	fi
done

sort ifdefs.txt > FINALE.TXT