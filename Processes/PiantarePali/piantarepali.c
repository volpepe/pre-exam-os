/* file:  piantarepali.c */

/* messo prima perche' contiene define _POSIX_C_SOURCE */
#include "printerror.h"
#include "DBGpthread.h"

#include <unistd.h>   /* exit() etc */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>     /* per strerror_r  and  memset */
#include <sys/mman.h>  /* shm_* stuff, and mmap() */
#include <sys/types.h>
#include <signal.h>
#include <sys/time.h>	/* timeval{} for select() */
#include <time.h>	/* timespec{} for pselect() */
#include <limits.h>	/* for OPEN_MAX */
#include <errno.h>
#include <sys/wait.h>
#include <assert.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdint.h>
#include <inttypes.h>

#include <fcntl.h>
#include <sys/stat.h>

#define PALI 2
#define OPERAI 2
#define PIANTATO 1
#define NON_PIANTATO 0
#define RIPOSO_MARTELLO 4
#define RIPOSO_TIENEPALO 5

typedef struct {
	int paliPiantati, paliPronti, palo[PALI];
	pthread_mutex_t mutex;
	pthread_cond_t condLavoroFinito, condIniziaLavoro;	
} Lavoro;

Lavoro* l;

void Martello(int i) {
	char ident[32];
	int cicli;
	/*i è anche l'indice del palo di cui ci occupiamo*/
	sprintf(ident, "Martello [%d]", i);
	
	while (1) {
		DBGpthread_mutex_lock(&l->mutex, ident);
		while (!l->paliPronti || l->palo[i]==PIANTATO) {
			/*entriamo qua dentro solo se ci tocca aspettare che arrivino
			 * i pali o se sono pronti ma il nostro palo è già stato
			 * piantato*/
			DBGpthread_cond_wait(&l->condIniziaLavoro, &l->mutex, ident);
			/*verremo svegliati quando i pali sono pronti*/			
		}
		
		/*arriviamo qua se i pali sono in posizione*/
		cicli++;
		DBGpthread_mutex_unlock(&l->mutex, ident);
		
		printf("%s inizia\n", ident); fflush(stdout);
		DBGsleep(1, ident);
		printf("%s finisce\n", ident); fflush(stdout);
		
		DBGpthread_mutex_lock(&l->mutex, ident);
		l->palo[i]=PIANTATO;
		l->paliPiantati++;
		/*se entrambi i pali sono stati piantati, lo facciamo sapere a TienePalo*/
		if (l->paliPiantati==PALI) DBGpthread_cond_signal(&l->condLavoroFinito, ident);
		DBGpthread_mutex_unlock(&l->mutex, ident);
		
		if(cicli%RIPOSO_MARTELLO==0) {
			/*se è il nostro quarto palo ci riposiamo*/
			printf("%s va in pausa\n", ident); fflush(stdout);
			DBGsleep(5+i*2, ident);
		}
	}
}

void TienePalo() {
	char ident[32];
	int cicli;

	sprintf(ident, "TieniPalo");
	
	while(1) {
		cicli++;
		printf("Presi pali\n"); fflush(stdout);
		DBGsleep(1, ident);
		printf("martellare!\n"); fflush(stdout);
		DBGpthread_mutex_lock(&l->mutex, ident);
		l->paliPiantati=0;
		l->paliPronti=1;
		l->palo[0]=NON_PIANTATO; l->palo[1]=NON_PIANTATO;
		/*chiamiamo i due operai dato che i pali sono pronti*/
		DBGpthread_cond_broadcast(&l->condIniziaLavoro, ident);
		/* e aspettiamo che abbiano finito entrambi*/
		DBGpthread_cond_wait(&l->condLavoroFinito, &l->mutex, ident);
		/*dopodichè ricominciamo*/
		DBGpthread_mutex_unlock(&l->mutex, ident);
		
		if(cicli%RIPOSO_TIENEPALO==0) {
			printf("%s va in pausa\n", ident); fflush(stdout);
			DBGsleep(3, ident);
		}		
	}
}

int main() {
	int shmfd, rc, i;
	pid_t pid;
	int shared_seg_size = sizeof(Lavoro);
	pthread_mutexattr_t mattr;
	pthread_condattr_t cvattr;

	/*si crea il segmento di memoria condivisa*/
	shmfd = shm_open( "/piantarepali", O_CREAT | O_RDWR, S_IRWXU );
	if (shmfd < 0) PrintERROR_andExit(errno, "error in shm_open");
	
	/*si imposta lo spazio del segmento*/
	rc = ftruncate(shmfd, shared_seg_size);
	if (rc != 0) PrintERROR_andExit(errno, "error in ftruncate");

	l = (Lavoro*)mmap(NULL, shared_seg_size,
		PROT_READ|PROT_WRITE, MAP_SHARED, shmfd, 0);
	if( l==MAP_FAILED )
	 		PrintERROR_andExit(errno,"mmap  failed");
	
	l->paliPiantati=0;
	l->paliPronti=0;
	l->palo[0]=NON_PIANTATO;
	l->palo[1]=NON_PIANTATO;

	rc=pthread_mutexattr_init(&mattr);
	if( rc ) PrintERROR_andExit(errno,"pthread_mutexattr_init failed");
	rc=pthread_mutexattr_setpshared(&mattr,PTHREAD_PROCESS_SHARED);
	if( rc ) PrintERROR_andExit(errno,"pthread_mutexattr_setpshared failed");
	rc=pthread_condattr_init(&cvattr);
	if( rc ) PrintERROR_andExit(errno,"pthread_condattr_init  failed");
	rc=pthread_condattr_setpshared(&cvattr,PTHREAD_PROCESS_SHARED);
	if( rc ) PrintERROR_andExit(errno,"pthread_condattr_setpshared failed");

	rc = pthread_cond_init(&l->condIniziaLavoro, &cvattr);
	if( rc ) PrintERROR_andExit(errno,"pthread_cond_init failed");
	rc = pthread_cond_init(&l->condLavoroFinito, &cvattr);
	if( rc ) PrintERROR_andExit(errno,"pthread_cond_init failed");
	rc = pthread_mutex_init(&l->mutex, &mattr); 
	if( rc ) PrintERROR_andExit(errno,"pthread_mutex_init failed");

	for(i=0; i<OPERAI ;i++) {
		pid=fork();
		if(pid<0) PrintERROR_andExit(errno,"fork failed");
		else if(pid==0) {
			/*figlio*/
			Martello(i);
			exit(0);
		}
	}
	TienePalo(); 
	
	return(0); 
}




