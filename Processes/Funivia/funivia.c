/* file:  funivia.c */

/* messo prima perche' contiene define _POSIX_C_SOURCE */
#include "printerror.h"
#include "DBGpthread.h"

#include <unistd.h>   /* exit() etc */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>     /* per strerror_r  and  memset */
#include <sys/mman.h>  /* shm_* stuff, and mmap() */
#include <sys/types.h>
#include <signal.h>
#include <sys/time.h>	/* timeval{} for select() */
#include <time.h>	/* timespec{} for pselect() */
#include <limits.h>	/* for OPEN_MAX */
#include <errno.h>
#include <sys/wait.h>
#include <assert.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdint.h>
#include <inttypes.h>

#include <fcntl.h>
#include <sys/stat.h>

#define POSTI_SOBRIO 2
#define POSTI_UBRIACO 1
#define UBRIACHI 2
#define SOBRI 4

typedef struct {
	int sobriSuFunivia, ubriachiSuFunivia, sobriFaiWait, ubriachiFaiWait;
	int postoA, postoB;
	pthread_mutex_t mutex;
	pthread_cond_t condFilaSobri, condFilaUbriachi, condInizioViaggio, condFineViaggio, condFuniviaLibera;
} FuniviaStruct;

FuniviaStruct* f;

void Ubriachi(int i) {
	char ident[32];
	
	sprintf(ident, "Ubriaco [%d]", i);
	
	while(1) {
		DBGpthread_mutex_lock(&f->mutex, ident);
		while (f->sobriSuFunivia>0 || f->ubriachiSuFunivia>=POSTI_UBRIACO || f->ubriachiFaiWait) {
			DBGpthread_cond_wait(&f->condFilaUbriachi, &f->mutex, ident);
		}
		
		/*qua posso entrare*/
		printf("%s entra\n", ident); fflush(stdout);
		f->ubriachiFaiWait=1;
		f->sobriFaiWait=1;
		f->ubriachiSuFunivia++;
		f->postoA=i;
		f->postoB=0;
		/*ci può essere un solo ubriaco quindi non sveglio nessuno e dico alla funivia
		 * di partire subito*/
		DBGpthread_cond_signal(&f->condInizioViaggio, ident);
		DBGpthread_cond_wait(&f->condFineViaggio, &f->mutex, ident);
		/*il viaggio è finito*/
		/*mi rimetto in fila*/
		f->ubriachiSuFunivia--;
		printf("%s esce\n",ident);
		DBGpthread_cond_signal(&f->condFuniviaLibera, ident);
		DBGpthread_mutex_unlock(&f->mutex, ident);	
	}
	pthread_exit(NULL);
}

void Sobri(int i) {
	char ident[32];
	
	sprintf(ident, "Sobrio [%d]", i);
	
	while(1) {
		DBGpthread_mutex_lock(&f->mutex, ident);
		while (f->ubriachiSuFunivia>0 || f->sobriSuFunivia>=POSTI_SOBRIO || f->sobriFaiWait) {
			DBGpthread_cond_wait(&f->condFilaSobri, &f->mutex, ident);
		}
		
		/*qua posso entrare*/
		printf("%s entra\n", ident); fflush(stdout);
		f->sobriSuFunivia++;
		/*ci possono essere due sobri e dico alla funivia
		 * di partire solo se sono il secondo*/
		if (f->sobriSuFunivia==POSTI_SOBRIO) {
			f->ubriachiFaiWait=1;
			f->sobriFaiWait=1;
			DBGpthread_cond_signal(&f->condInizioViaggio, ident);
			f->postoB=i;
		} else {
			f->postoA=i;
		}
		DBGpthread_cond_wait(&f->condFineViaggio, &f->mutex, ident);
		/*il viaggio è finito*/
		/*mi rimetto in fila*/
		f->sobriSuFunivia--;
		printf("%s esce\n",ident);
		if (f->sobriSuFunivia==0) DBGpthread_cond_signal(&f->condFuniviaLibera, ident);
		DBGpthread_mutex_unlock(&f->mutex, ident);
		
	}
	pthread_exit(NULL);
}

void Funivia() {
	char ident[32];
	
	sprintf(ident, "Funivia");
	
	while(1) {
		DBGpthread_mutex_lock(&f->mutex, ident);
		if (f->sobriSuFunivia<POSTI_SOBRIO && f->ubriachiSuFunivia<POSTI_UBRIACO) {
			DBGpthread_cond_wait(&f->condInizioViaggio, &f->mutex, ident);
		}
		/*partiamo*/
		if (f->sobriSuFunivia>0) {
			printf("Sulla funivia ci sono due sobri: %d e %d\n", f->postoA, f->postoB); fflush(stdout);
		} else {
			printf("Sulla funivia c'è un ubriaco: %d\n", f->postoA); fflush(stdout);
		}
		
		DBGpthread_mutex_unlock(&f->mutex, ident);
		DBGsleep(1, ident); DBGsleep(1, ident);
		DBGpthread_mutex_lock(&f->mutex, ident);
		/*ho fatto il viaggio*/
		DBGpthread_cond_broadcast(&f->condFineViaggio, ident);
		DBGpthread_cond_wait(&f->condFuniviaLibera, &f->mutex, ident);
		DBGpthread_cond_broadcast(&f->condFilaSobri, ident);
		DBGpthread_cond_broadcast(&f->condFilaUbriachi, ident);
		f->ubriachiFaiWait=0;
		f->sobriFaiWait=0;
		DBGpthread_mutex_unlock(&f->mutex, ident);	
	}
	pthread_exit(NULL);
}

int main() { 
	int shmfd, rc, i;
	pid_t pid;
	int shared_seg_size = sizeof(FuniviaStruct);
	pthread_mutexattr_t mattr;
	pthread_condattr_t cvattr;

	shmfd = shm_open( "/funivia", O_CREAT | O_RDWR, S_IRWXU );
	if (shmfd < 0) PrintERROR_andExit(errno,"shm_open failed");
	
	/* ridimensiono il segmento di memoria condivisa appena creato*/
	rc = ftruncate(shmfd, shared_seg_size);
	if (rc != 0) PrintERROR_andExit(errno,"ftruncate failed");

	f = (FuniviaStruct*)mmap(NULL, shared_seg_size,
		PROT_READ|PROT_WRITE, MAP_SHARED, shmfd, 0);
	if( f==MAP_FAILED )
	 		PrintERROR_andExit(errno,"mmap  failed");
	
	f->sobriSuFunivia=0; f->ubriachiSuFunivia=0; f->sobriFaiWait=0; f->ubriachiFaiWait=0;

	rc=pthread_mutexattr_init(&mattr);
	if( rc ) PrintERROR_andExit(errno,"pthread_mutexattr_init  failed");
	rc=pthread_mutexattr_setpshared(&mattr,PTHREAD_PROCESS_SHARED);
	if( rc ) PrintERROR_andExit(errno,"pthread_mutexattr_setpshared  failed");
	
	rc=pthread_condattr_init(&cvattr);
	if( rc ) PrintERROR_andExit(errno,"pthread_condattr_init  failed");
	rc=pthread_condattr_setpshared(&cvattr,PTHREAD_PROCESS_SHARED);
	if( rc ) PrintERROR_andExit(errno,"pthread_condattr_setpshared  failed");


	rc = pthread_cond_init(&f->condFilaSobri, &cvattr);
	if( rc ) PrintERROR_andExit(errno,"pthread_cond_init failed");
	rc = pthread_cond_init(&f->condFilaUbriachi, &cvattr);
	if( rc ) PrintERROR_andExit(errno,"pthread_cond_init failed");
	rc = pthread_cond_init(&f->condFineViaggio, &cvattr);
	if( rc ) PrintERROR_andExit(errno,"pthread_cond_init failed");
	rc = pthread_cond_init(&f->condInizioViaggio, &cvattr);
	if( rc ) PrintERROR_andExit(errno,"pthread_cond_init failed");
	rc = pthread_cond_init(&f->condFuniviaLibera, &cvattr);
	if( rc ) PrintERROR_andExit(errno,"pthread_cond_init failed");

	rc = pthread_mutex_init(&f->mutex, &mattr); 
	if( rc ) PrintERROR_andExit(errno,"pthread_mutex_init failed");


	for(i=0;i<UBRIACHI+SOBRI;i++) {
		pid=fork();
		if(pid<0) 
			PrintERROR_andExit(errno,"fork failed");
		else if(pid==0) {
			/*figlio*/
			if( i<UBRIACHI )
				Ubriachi(i);
			else 
				Sobri(i);
			exit(0);
		}
	}
	Funivia(); 
	
	return(0); 
} 
