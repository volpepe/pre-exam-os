/* file:  guadoZambesi.c */

/* messo prima perche' contiene define _POSIX_C_SOURCE */
#include "printerror.h"
#include "DBGpthread.h"

#include <unistd.h>   /* exit() etc */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>     /* per strerror_r  and  memset */
#include <sys/mman.h>  /* shm_* stuff, and mmap() */
#include <sys/types.h>
#include <signal.h>
#include <sys/time.h>	/* timeval{} for select() */
#include <time.h>	/* timespec{} for pselect() */
#include <limits.h>	/* for OPEN_MAX */
#include <errno.h>
#include <sys/wait.h>
#include <assert.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdint.h>
#include <inttypes.h>

#include <fcntl.h>
#include <sys/stat.h>

#define N_IPPO 2
#define N_LEMURI 7
#define POSTI 4

typedef struct {
	int ippopotamoEntra, ippopotamoInAcqua, lemuriSuIppo, ippoInFila, lemuriInFila;
	/*lemuriSuIppo è da intendersi come la variabile che mantiene il numero di lemuri 
	 * sull'ippopotamo attualmente in acqua*/
	pthread_mutex_t mutex;
	pthread_cond_t condFilaLemuri, condFilaIppo, condIppo;
} Fiume;

Fiume* f;

void *Lemure(int i) {
	char ident[32];
	
	sprintf(ident, "Lemure [%d]", i);
	
	while(1) {
		DBGpthread_mutex_lock(&f->mutex, ident);
		while(!f->ippopotamoEntra || f->lemuriSuIppo>=POSTI) {
			/*ci tocca aspettarlo*/
			f->lemuriInFila++;
			DBGpthread_cond_wait(&f->condFilaLemuri, &f->mutex, ident);
			f->lemuriInFila--;
		}
		
		f->lemuriSuIppo++;
		printf("%s sale sull'ippopotamo in acqua\n", ident); fflush(stdout);
		DBGpthread_cond_wait(&f->condIppo, &f->mutex, ident);
		
		/*veniamo risvegliati quando si deve scendere*/
		/*lemuriInFila viene già impostata a 0 dall'ippopotamo che ci sveglia*/
		DBGpthread_mutex_unlock(&f->mutex, ident);
		
		printf("%s è sollevato da una tromba d'aria\n", ident);
		DBGsleep(3, ident);
	}
	pthread_exit(NULL);	
}

void Ippopotamo(int i) {
	char ident[32];
	
	sprintf(ident, "Ippopotamo [%d]", i);
	
	while(1) {
		DBGpthread_mutex_lock(&f->mutex, ident);
		printf("%s arrivato sulla prima riva\n", ident);
		while (f->ippopotamoInAcqua) {
			f->ippoInFila++;
			DBGpthread_cond_wait(&f->condFilaIppo, &f->mutex, ident);
			f->ippoInFila--;
		}
		
		f->lemuriSuIppo=0;
		/*possiamo entrare in acqua*/
		f->ippopotamoInAcqua=1;
		f->ippopotamoEntra=1;
		if (f->lemuriInFila>0) DBGpthread_cond_broadcast(&f->condFilaLemuri, ident);
		DBGpthread_mutex_unlock(&f->mutex, ident);
		/*entriamo in acqua*/
		DBGsleep(2, ident);
		DBGpthread_mutex_lock(&f->mutex, ident);
		f->ippopotamoEntra=0; /*siamo in acqua*/
		printf("%s entra in acqua trasportando %d lemuri\n", ident, f->lemuriSuIppo);
		fflush(stdout);
		
		DBGpthread_mutex_unlock(&f->mutex, ident);
		/*viaggio*/
		DBGsleep(3, ident);
		DBGpthread_mutex_lock(&f->mutex, ident);
		
		printf("%s arrivato sull'altra riva\n", ident);
		f->ippopotamoInAcqua=0;
		/*sveglio i lemuri che ho in spalla*/
		DBGpthread_cond_broadcast(&f->condIppo, ident);
		/*sveglio l'altro ippopotamo perchp non sono più in acqua e volendo può entrare lui*/
		if (f->ippoInFila>0) DBGpthread_cond_signal(&f->condFilaIppo, ident);
		DBGpthread_mutex_unlock(&f->mutex, ident);
		
		printf("%s è sollevato da una tromba d'aria\n", ident);
		DBGsleep(3, ident);
	}
	pthread_exit(NULL);
}

int main() {
	int shmfd, rc, i;
	pid_t pid;
	int shared_seg_size = sizeof(Fiume);
	pthread_mutexattr_t mattr;
	pthread_condattr_t cvattr;

	shmfd = shm_open( "/guadoZambesi", O_CREAT /*| O_EXCL*/ | O_RDWR, S_IRWXU );
	if (shmfd < 0) {
		perror("In shm_open()");
		exit(1);
	}
	/* adjusting mapped file size (make room for the whole segment to map) */
	rc = ftruncate(shmfd, shared_seg_size);
	if (rc != 0) {
		perror("ftruncate() failed");
		exit(1);
	}

	f = (Fiume*)mmap(NULL, shared_seg_size,
		PROT_READ|PROT_WRITE, MAP_SHARED, shmfd, 0);
	if( f==MAP_FAILED )
	 		PrintERROR_andExit(errno,"mmap  failed");
	
	f->ippopotamoEntra=0; f->ippopotamoInAcqua=0; f->lemuriSuIppo=0;
	f->ippoInFila=0; f->lemuriInFila=0;

	rc=pthread_mutexattr_init(&mattr);
	if( rc ) PrintERROR_andExit(errno,"pthread_mutexattr_init  failed");
	rc=pthread_mutexattr_setpshared(&mattr,PTHREAD_PROCESS_SHARED);
	if( rc ) PrintERROR_andExit(errno,"pthread_mutexattr_setpshared  failed");
	rc=pthread_condattr_init(&cvattr);
	if( rc ) PrintERROR_andExit(errno,"pthread_condattr_init  failed");
	rc=pthread_condattr_setpshared(&cvattr,PTHREAD_PROCESS_SHARED);
	if( rc ) PrintERROR_andExit(errno,"pthread_condattr_setpshared  failed");


	rc = pthread_cond_init(&f->condFilaIppo, &cvattr);
	if( rc ) PrintERROR_andExit(errno,"pthread_cond_init failed");
	rc = pthread_cond_init(&f->condFilaLemuri, &cvattr);
	if( rc ) PrintERROR_andExit(errno,"pthread_cond_init failed");
	rc = pthread_cond_init(&f->condIppo, &cvattr);
	if( rc ) PrintERROR_andExit(errno,"pthread_cond_init failed");

	rc = pthread_mutex_init(&f->mutex, &mattr); 
	if( rc ) PrintERROR_andExit(errno,"pthread_mutex_init failed");


	for(i=0;i<N_LEMURI;i++) {
		pid=fork();
		if(pid<0) 
			PrintERROR_andExit(errno,"fork failed");
		else if(pid==0) {
			Lemure(i);
			exit(0);
		}
	}
	
	pid=fork();
	if(pid<0) PrintERROR_andExit(errno,"fork failed");
	else if (pid==0) {
		Ippopotamo(0);
		exit (0);
	} else {
		Ippopotamo(1);
		exit (0);
	}
	
	return(0); 
}

