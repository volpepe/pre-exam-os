#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

#include <signal.h>
#include <sys/time.h>	/* timeval{} for select() */
#include <time.h>	/* timespec{} for pselect() */
#include <string.h>     /* per strerror_r  and  memset */
#include <limits.h>	/* for OPEN_MAX */
#include <errno.h>
#include <sys/wait.h>

#include "printerror.h"

#define NPROC 10

int main(void) {
	int index;
	pid_t pid;

	for (index=0; index<NPROC; index++) {
		pid=fork();	
		if(pid<0)
		{
			fprintf(stderr, "fork() failed: error %d\n", errno ); /*scrive su stderr invece che su stdout*/
			fflush(stderr);
			exit(1);
		}
		else if(pid==0) /* il figlio */
		{
			printf("Sono il figlio di indice %d e termino\n", index);
			exit(0);
		}
	}
	for (index=0; index<NPROC; index++) {
		wait(NULL);
	}

	return 0;
}