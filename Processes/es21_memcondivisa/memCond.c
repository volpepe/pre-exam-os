#include <stdlib.h>
#include <unistd.h>   /* exit() etc */
#include <stdio.h>
#include <string.h>   /* memset */
#include <sys/mman.h>  /* shm_* stuff, and mmap() */
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/wait.h>

#include "printerror.h"

#define SHMOBJ_PATH         "/es21"
#define SHARED_SEGMENT_SIZE 512

int main (void) {
	int shmfd, rc, status;
	pid_t pid, childpid;
    int shared_seg_size = SHARED_SEGMENT_SIZE;
    char *shared_msg;

    /*1*/
    shmfd = shm_open(SHMOBJ_PATH, O_CREAT | O_RDWR, S_IRWXU);
    if (shmfd < 0) {
        perror("shm_open() failed");
        exit(1);
    }
    printf("Created shared memory object %s\n", SHMOBJ_PATH);

    /*2*/
    rc = ftruncate(shmfd, shared_seg_size);
    if (rc != 0) {
        perror("ftruncate() failed");
        exit(1);
    }

    /* requesting the shared segment */    
    shared_msg = (char *)mmap(NULL, shared_seg_size, PROT_READ | PROT_WRITE, MAP_SHARED, shmfd, 0);
    if (shared_msg == MAP_FAILED) {
        perror("mmap() failed");
        exit(1);
    }

    memset(shared_msg, 'B', shared_seg_size );
    shared_msg[shared_seg_size-1]='\0';
    printf("segmento: %s\n", shared_msg);

	pid=fork();
	if(pid<0)
	{
		fprintf(stderr, "fork() failed: error %d\n", errno ); /*scrive su stderr invece che su stdout*/
		fflush(stderr);
		exit(1);
	}
	else if(pid==0) /* il figlio */ {
		memset(shared_msg, 'K', shared_seg_size );
    	shared_msg[shared_seg_size-1]='\0';

    	/**what could go wrong???*/
    	exit(0);
	} else {
		/*father*/
		do {
			childpid = waitpid ( pid, &status, 0 );
		} while( (childpid<0) && (errno==EINTR) );

		rc =  WEXITSTATUS(status);

		if(!rc) {
			printf("padre: figlio termina ok!\n");
			printf("segmento: %s\n", shared_msg);
			fflush(stdout);
		}
		else {
			printf("figlio termina in modo anormale: %d\n", rc);
			fflush(stdout);
		}

    	/* requesting the removal of the shm object */
    	if (shm_unlink(SHMOBJ_PATH) != 0) {
    	    perror("shm_unlink() failed");
    	    exit(1);
   		}
	}
	return 0;
}