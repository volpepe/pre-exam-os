/*counting.c*/

#include "printerror.h"
#include "DBGpthread.h"

#include <stdlib.h>
#include <unistd.h>   /* exit() etc */
#include <stdio.h>
#include <string.h>     /* per strerror_r  and  memset */
#include <sys/mman.h>  /* shm_* stuff, and mmap() */
#include <sys/types.h>
#include <signal.h>
#include <sys/time.h>	/* timeval{} for select() */
#include <time.h>	/* timespec{} for pselect() */
#include <limits.h>	/* for OPEN_MAX */
#include <errno.h>
#include <sys/wait.h>
#include <assert.h>
#include <pthread.h>
#include <fcntl.h>
#include <sys/stat.h>

#define NUM_PROC 10
#define ASP_PROC 5

typedef struct {
	int Count;
	pthread_mutex_t mutex;
} Counter;

Counter* counter;

int main() {
	int shmfd, rc, i;
	pid_t pid;
	int shared_seg_size = sizeof(Counter);
	pthread_mutexattr_t mattr;
	
	/*creiamo il segmento di shared memory*/
	shmfd = shm_open( "/counter", O_CREAT | O_RDWR, S_IRWXU );
    if (shmfd < 0) PrintERROR_andExit(errno, "shm_open failed");

    /*impostiamo la sua dimensione*/
    rc = ftruncate(shmfd, shared_seg_size);
    if (rc) PrintERROR_andExit(errno, "ftruncate failed");

    /*ora abbiamo un segmento di memoria condivisa di dimensione sizeof(Counter):
    dobbiamo renderlo parte del nostro spazio di indirizzamento*/
    counter = (Counter *)mmap(NULL, sizeof(Counter),
        PROT_READ|PROT_WRITE, MAP_SHARED, shmfd, 0);

    /*inizializzazione campi*/
    rc=pthread_mutexattr_init(&mattr);
    if( rc ) PrintERROR_andExit(errno,"pthread_mutexattr_init  failed");
    rc=pthread_mutexattr_setpshared(&mattr,PTHREAD_PROCESS_SHARED);
    if( rc ) PrintERROR_andExit(errno,"pthread_mutexattr_setpshared  failed");
	rc = pthread_mutex_init(&counter->mutex, &mattr);
	if (rc) PrintERROR_andExit(errno, "init failed");

	counter->Count=0;

	for(i=0; i<NUM_PROC; i++)
	{
		pid=fork();
		if(pid<0) { 
	        PrintERROR_andExit(errno,"fork() failed ");
	    }
	    if (pid==0)
	    {
	    	/*figlio*/
	    	char ident[32];

	    	sprintf(ident, "figlio %d", i);

	    	DBGpthread_mutex_lock(&counter->mutex, ident);
	    	printf("figlio incrementa\n"); fflush(stdout);
	    	counter->Count++;
	    	if (counter->Count==NUM_PROC+1) {
				printf("Count vale %d\n", counter->Count); fflush(stdout);
				DBGpthread_mutex_unlock(&counter->mutex, "main");
				if (shm_unlink("/counter") != 0) {
		   			printf("shm_unlink() failed\n");
		    		exit(1);
				}
				printf("Oggetto unlinkato\n"); fflush(stdout);
			}
	    	DBGpthread_mutex_unlock(&counter->mutex, ident);
	    	exit(0);
	    }
	    /*padre: non fa niente*/
	}

	for(i=0; i<ASP_PROC; i++) { wait(NULL); printf("padre fa join\n"); };
	DBGpthread_mutex_lock(&counter->mutex, "padre");
	printf("padre incrementa\n"); fflush(stdout);
	counter->Count++;
	DBGpthread_mutex_unlock(&counter->mutex, "padre");

	if (counter->Count==NUM_PROC+1) {
		printf("Count vale %d\n", counter->Count); fflush(stdout);
		/* requesting the removal of the shm object */
		DBGpthread_mutex_unlock(&counter->mutex, "main");
		if (shm_unlink("/counter") != 0) {
	   		printf("shm_unlink() failed\n");
	    	exit(1);
		}
	 }

	return 0;

}
