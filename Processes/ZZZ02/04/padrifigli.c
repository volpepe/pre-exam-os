/*padrifigli.c*/

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

#include <signal.h>
#include <sys/time.h>	/* timeval{} for select() */
#include <time.h>	/* timespec{} for pselect() */
#include <string.h>     /* per strerror_r  and  memset */
#include <limits.h>	/* for OPEN_MAX */
#include <errno.h>
#include <sys/wait.h>

#include "printerror.h"

int main() {
	pid_t pid;
	int index, ret, status;

	for(index=0; index<9; index++) {

		pid=fork();
		if(pid<0)
		{
			fprintf(stderr, "Fork() failed: error %d\n", errno); exit(-1);
		}
		if (pid>0) 
		{
			/*padre*/
			/*ogni processo crea un figlio, ne aspetta la terminazione
			e restituisce il suo exit code +1*/
			pid_t childpid;

			do {
				childpid = waitpid(pid, &status, 0);
			} while((childpid<0) && (errno==EINTR));

			ret=WEXITSTATUS(status)+1;

			printf("Figlio %d restituisce %d\n", index, ret); fflush(stdout);
			exit(ret);
		}
		else {
			/*figlio*/
			if(index==8) {
				printf("Sono il decimo figlio e restituisco 1\n"); exit(1);
			}
		}
	}
	return 0;
}