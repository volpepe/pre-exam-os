/*flinstones.c complicato perchè fatto con i processi*/

/* messo prima perche' contiene define _POSIX_C_SOURCE */
#include "printerror.h"
#include "DBGpthread.h"

#include <stdlib.h>
#include <unistd.h>   /* exit() etc */
#include <stdio.h>
#include <string.h>     /* per strerror_r  and  memset */
#include <sys/mman.h>  /* shm_* stuff, and mmap() */
#include <sys/types.h>
#include <signal.h>
#include <sys/time.h>	/* timeval{} for select() */
#include <time.h>	/* timespec{} for pselect() */
#include <limits.h>	/* for OPEN_MAX */
#include <errno.h>
#include <sys/wait.h>
#include <assert.h>
#include <pthread.h>
#include <fcntl.h>
#include <sys/stat.h>

#define A 0
#define B 1
#define NSTARTCAVERN_A 2
#define NSTARTCAVERN_B 1

/*definiamo la struttura dati. in un esercizio con i processi la struttura dati
contiene tutti quei dati che normalmente risulterebbero globali*/
typedef struct {
	int latoCoda;
	int postiLiberi;
	pthread_mutex_t mutex;
	pthread_cond_t condDino,
				condSide[2],
				condParto,
				condScendete;
} dinosaur_t;

/*quello che è globale è il puntatore alla struttura dati*/
dinosaur_t *d;

void Dinosauro(void) {
	char ident[32];

	sprintf(ident, "Dinosauro");

	while(1) {
		DBGpthread_mutex_lock(&d->mutex, ident);

		/*devo aspettare che salga gente e partire solo quando sono in due
		è solo il secondo a farmi la signal, quindi:*/
		if (d->postiLiberi>0) {
			DBGpthread_cond_wait(&d->condParto, &d->mutex, ident);
		}

		/*se siamo qua ci sono due cavernicoli sul mio groppone*/
		printf("Dinosauro parte\n"); fflush(stdout);
		DBGpthread_mutex_unlock(&d->mutex, ident);
		DBGsleep(2, ident);
		DBGpthread_mutex_lock(&d->mutex, ident);
		printf("Arrivati!\n"); fflush(stdout);
		/*faccio scendere i cavernicoli*/
		d->latoCoda=(d->latoCoda+1)%2;
		DBGpthread_cond_broadcast(&d->condDino, ident);
		/*aspetto che siano scesi*/
		DBGpthread_cond_wait(&d->condScendete, &d->mutex, ident);
		/*quando sono scesi tutti chiamo gli altri su quel lato*/
		DBGpthread_cond_broadcast(&d->condSide[d->latoCoda], ident);
		/*il mio lavoro è finito: ricomincio ad aspettare*/
		DBGpthread_mutex_unlock(&d->mutex, ident);
	}

	pthread_exit(NULL);
}

void Cavernicolo(int i, int side) {
	char ident[32];

	sprintf(ident, "Cavernicolo %d", i);

	while(1) {
		DBGpthread_mutex_lock(&d->mutex, ident);

		/*guardiamo se siamo sul lato giusto per salire e se ci sono posti liberi*/
		while (side!=d->latoCoda || d->postiLiberi<=0) {
			/*se il lato è sbagliato aspetto*/
			DBGpthread_cond_wait(&d->condSide[side], &d->mutex, ident);
		}

		/*se arriviamo qui vuol dire che il lato è quello giusto*/
		/*dato che ci sono posti liberi saliamo sul dinosauro*/
		d->postiLiberi--;
		/*se sono il secondo a salire dico al dinosauro di partire*/
		if (d->postiLiberi==0) DBGpthread_cond_signal(&d->condParto, ident);
		/*mi metto ad aspettare di arrivare di là*/
		printf("%s dal lato %c ha preso posto sul dinosauro\n", ident, side==A? 'A' : 'B'); fflush(stdout);
		DBGpthread_cond_wait(&d->condDino, &d->mutex, ident);

		/*quando il dinosauro arriva di là vengo svegliato e devo scendere*/
		side=(side+1)%2; /*sono sull'altro lato*/
		d->postiLiberi++;
		/*se sono l'ultimo dico al dinosauro che sono sceso*/
		if ( d->postiLiberi==2 ) DBGpthread_cond_signal(&d->condScendete, ident);
		/*vado a fare un giro*/
		DBGpthread_mutex_unlock(&d->mutex, ident);

		printf("%s scende sul lato %c\n", ident, side==A? 'A' : 'B'); fflush(stdout);
		DBGsleep(4, ident);
	}

	pthread_exit(NULL);
}

int main() {
    int shmfd, rc, i;
    pid_t pid;
    int shared_seg_size = sizeof(dinosaur_t);
    pthread_mutexattr_t mattr;
    pthread_condattr_t cvattr;

    /*creiamo il segmento di shared memory*/
    shmfd = shm_open( "/d", O_CREAT | O_RDWR, S_IRWXU );
    if (shmfd < 0) {
        perror("In shm_open()");
        exit(1);
    }
    
    /*impostiamo la sua dimensione*/
    rc = ftruncate(shmfd, shared_seg_size);
    if (rc != 0) {
        perror("ftruncate() failed");
        exit(1);
    }

    /*ora abbiamo un segmento di memoria condivisa di dimensione sizeof(dinosaur_t):
    dobbiamo renderlo parte del nostro spazio di indirizzamento*/
    d = (dinosaur_t *)mmap(NULL, sizeof(dinosaur_t),
        PROT_READ|PROT_WRITE, MAP_SHARED, shmfd, 0);
    d->latoCoda=A;
    d->postiLiberi=2;

    /*ora inizializziamo tutte le mutex e cond*/
    rc=pthread_mutexattr_init(&mattr);
    if( rc ) PrintERROR_andExit(errno,"pthread_mutexattr_init  failed");
    rc=pthread_mutexattr_setpshared(&mattr,PTHREAD_PROCESS_SHARED);
    if( rc ) PrintERROR_andExit(errno,"pthread_mutexattr_setpshared  failed");
    rc=pthread_mutex_init(&d->mutex, &mattr);
    if( rc ) PrintERROR_andExit(errno,"pthread_mutex_init  failed");

    rc=pthread_condattr_init(&cvattr);
    if( rc ) PrintERROR_andExit(errno,"pthread_condattr_init  failed");
    rc=pthread_condattr_setpshared(&cvattr,PTHREAD_PROCESS_SHARED);
    if( rc ) PrintERROR_andExit(errno,"pthread_condattr_setpshared  failed");
    rc=pthread_cond_init(&d->condDino, &cvattr);
    if( rc ) PrintERROR_andExit(errno,"pthread_cond_init  failed");
    rc=pthread_cond_init(&d->condSide[0], &cvattr);
    if( rc ) PrintERROR_andExit(errno,"pthread_cond_init  failed");
    rc=pthread_cond_init(&d->condSide[1], &cvattr);
    if( rc ) PrintERROR_andExit(errno,"pthread_cond_init  failed");
    rc=pthread_cond_init(&d->condParto, &cvattr);
    if( rc ) PrintERROR_andExit(errno,"pthread_cond_init  failed");
    rc=pthread_cond_init(&d->condScendete, &cvattr);
    if( rc ) PrintERROR_andExit(errno,"pthread_cond_init  failed");
    /*fatto: ora dobbiamo solo creare i processi*/

    for (i=0; i<(NSTARTCAVERN_B+NSTARTCAVERN_A); i++) {
	    pid = fork();
	    if(pid<0) { 
	        PrintERROR_andExit(errno,"fork() failed ");
	    } else if (pid==0) {
	    	/*figlio*/
	    	if (i<NSTARTCAVERN_A)
	    		Cavernicolo(i, A);
	    	else Cavernicolo(i, B);
	    	exit(0);
	    }
	}
	Dinosauro();

    return 0;
}
